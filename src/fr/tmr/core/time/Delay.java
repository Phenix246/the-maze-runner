package fr.tmr.core.time;

public class Delay
{
	private int length;
	private long endTime;
	private boolean started;

	public Delay(int millis)
	{
		this.length = millis;
		this.started = false;
	}

	public boolean isOver()
	{
		if(!this.started)
			return false;
		return Time.getMillisTime() >= endTime;
	}

	public boolean isStarted()
	{
		return this.started;
	}

	public void start()
	{
		this.started = true;
	}

	public void stop()
	{
		this.started = false;
	}

	public void restart()
	{
		this.endTime = length + Time.getMillisTime();
		this.start();
	}

	public void terminate()
	{
		started = true;
		endTime = 0;
	}
}