package fr.tmr.core.time;

public class Chronometers
{
	private long startTime;
	private boolean started;
	private long length;

	public Chronometers(int millis)
	{
		this.length = millis;
		this.started = false;
	}

	public Chronometers()
	{
		this(0);
	}

	public long getTime()
	{
		return(Time.getMillisTime() - this.startTime);
	}

	public boolean isStarted()
	{
		return this.started;
	}

	public void start()
	{
		this.started = true;
		this.startTime = Time.getMillisTime() + length;
	}

	public void stop()
	{
		this.started = false;
	}

	public void restart()
	{
		this.start();
	}

}
