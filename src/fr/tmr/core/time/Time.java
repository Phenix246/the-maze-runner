package fr.tmr.core.time;

public class Time
{
	public static long getNanoTime()
	{
		return System.nanoTime();
	}

	public static long getMillisTime()
	{
		return System.currentTimeMillis();
	}

}
