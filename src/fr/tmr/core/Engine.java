package fr.tmr.core;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class Engine
{
	public static Game game;

	// Variables de cap pour les fps et tps
	public static int fpsCap = 60;
	public static int tpsCap = 20;

	public static int ticks, frames;

	// Variables de la largeur et de la hauteur de la fenêtre
	public static int width, height;

	public static Logger logger;

	public static boolean isRunning = true;
	public static boolean debug = true;

	// Variables système
	private static String javaVersion, osVersion, osName, osArch;
	public static String path;

	public static Thread threadUpdate, threadRender;

	public static void setFpsCap(int fps)
	{
		Engine.fpsCap = fps;
	}

	public static void setTpsCap(int tps)
	{
		Engine.tpsCap = tps;
	}

	public static void setIcons(ByteBuffer[] icons)
	{
		// Définie les icones de la fenêtre
		if(icons != null && icons.length == 2)
			Display.setIcon(icons);
	}

	public static void initEngine(int width, int height, boolean isResizable, boolean isFullscreenn, String title, Game game)
	{
		System.out.println("Start engine");
		// Initialisation de variable de système
		Engine.javaVersion = System.getProperty("java.version");
		Engine.osVersion = System.getProperty("os.version");
		Engine.osName = System.getProperty("os.name");
		Engine.osArch = System.getProperty("os.arch");
		Engine.path = System.getProperty("user.dir");

		System.out.println();

		// Initialize le logger
		logger = Logger.getLogger(Engine.class.getName());

		Engine.game = game;

		// Initialisation de l'écran
		Engine.initDisplay(width, height, isResizable, isFullscreenn, title);

		// Initialisation du jeu
		Engine.game.init();
	}

	public static void engineLoop()
	{
		System.out.println("Run game");
		// Boucle principale du jeu
		loop();

		// Ferme le jeu et détruit toutes instances
		cleanUp();
	}

	// Ferme le jeu et détruit toutes instances
	public static void cleanUp()
	{
		Display.destroy();
		Keyboard.destroy();
		Mouse.destroy();
	}

	public static void loop()
	{

		// Engine.threadUpdate = new ThreadUpdate("Update");
		// Engine.threadRender = new ThreadRender("Render");

		// Engine.threadUpdate.start();
		// Engine.threadRender.start();

		// timer variables
		long timer = System.currentTimeMillis();
		long lastTime = System.nanoTime();

		double nanoSeconds = 1_000_000_000.0D / 60.0D;
		double passed = 0;

		int ticks = 0;
		int frames = 0;

		// Tant que le jeu doit continuer de tourner
		while(isRunning)
		{
			// Si on désir fermer la fenêtre, définir la variable isRunning comme fausse
			if(Display.isCloseRequested())
			{
				Engine.game.close();
				Engine.isRunning = false;
			}

			// Update l'écran
			Display.update();

			// Calcul le temps écouler entre deux opérations
			long now = System.nanoTime();
			passed = now - lastTime;
			lastTime = now;

			// Si le temps passer est supérieur à nanoSeconds
			if(passed > nanoSeconds)
			{
				lastTime += nanoSeconds;

				// TODO fix : harmoniser le nombre de tps (cf : nanoseconds)
				// Update le jeu
				Engine.update(ticks);

				// Ajoute 1 au nombre de ticks
				ticks++;

			}
			else
			{
				// Définie les nombre maximum de rendu par seconde
				Display.sync(fpsCap);

				// Rend le jeu
				Engine.render();

				// Ajoute 1 au nombre de frame
				frames++;
			}

			// Chaque seconde afficher le nombre de tcks et de frames
			if(System.currentTimeMillis() - timer > 1_000)
			{
				timer += 1_000;

				// on change le nombre de tcks et de frames pour ce cycle
				Engine.ticks = ticks;
				Engine.frames = frames;

				// Remet à zéro le nombre de ticks et de frames
				ticks = 0;
				frames = 0;
			}
		}
	}

	public static void update(int ticks)
	{
		Engine.game.update(ticks);
	}

	private static void render()
	{
		// Obtient la nouvelle largeur de l'écran
		Engine.width = Display.getWidth();

		// Obtient la nouvelle heuteur de l'écran
		Engine.height = Display.getHeight();

		// Met à jour les informations de Open GL
		view2D(Engine.width, Engine.height);

		// Efface le contenu de l'écran
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

		// Rend le jeu
		Engine.game.render();
	}

	// Initialisation de l'écran
	private static void initDisplay(int width, int height, boolean isResizable, boolean isFullscreenn, String title)
	{
		try
		{
			// Initialisation du mode de l'écran de jeu (taille)
			Display.setDisplayMode(new DisplayMode(width, height));

			// Rend possible le redimensionnage de l'écran
			Display.setResizable(isResizable);

			// désactivation du pleine écran
			Display.setFullscreen(isFullscreenn);

			// Définition du nom de la fenêtre
			Display.setTitle(title);

			// Création de la fenêtre
			Display.create();

			// Création du clavier pour le jeu
			Keyboard.create();

			// Création de la souris pour le jeu
			Mouse.create();

			// Chargement de Open GL
			view2D(width, height);

			logger.info("test");
		}
		catch(LWJGLException e)
		{
			logger.log(Level.SEVERE, null, e);
		}
	}

	// Init Open GL
	public static void view2D(int width, int height)
	{
		GL11.glViewport(0, 0, width, height);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();

		GL11.glOrtho(0, width, height, 0, 1, -1);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		GL11.glEnable(GL11.GL_TEXTURE_2D);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

	}
}
