package fr.tmr.core.gui;

import org.lwjgl.opengl.GL11;

import fr.tmr.core.color.Color;
import fr.tmr.core.graphics.Renderer;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.gui.GuiScreen;

public class Gui
{
	public static void displayGui(GuiScreen gui)
	{
		gui.init();
		gui.render();
		TheMazeRunner.instance.currentScreen = gui;
	}

	public void drawString(int xPos, int yPos, String text)
	{
		Renderer.drawString(text, xPos, yPos, 20);
	}

	public void drawString(int xPos, int yPos, String text, Color color)
	{
		color.color();
		Renderer.drawString(text, xPos, yPos, 20, false, true);
		Color.WHITE.color();
	}

	public void drawString(int xPos, int yPos, String text, int size)
	{
		Renderer.drawString(text, xPos, yPos, size);
	}

	public void drawString(int xPos, int yPos, String text, int size, Color color)
	{
		color.color();
		Renderer.drawString(text, xPos, yPos, size, false, true);
		Color.WHITE.color();
	}

	public void drawCenteredString(int xPos, int yPos, String text)
	{
		Renderer.drawString(text, xPos, yPos, 20, true);
	}

	public void drawCenteredString(int xPos, int yPos, String text, int size)
	{
		Renderer.drawString(text, xPos, yPos, size, true);
	}

	public void drawCenteredString(int xPos, int yPos, String text, int size, Color color)
	{
		color.color();
		Renderer.drawString(text, xPos, yPos, size, true, true);
		Color.WHITE.color();
	}

	public void drawRect(int x, int y, float width, float height)
	{
		GL11.glPushMatrix();
		{
			GL11.glBegin(GL11.GL_QUADS);
			{
				GL11.glTexCoord2d(0, 0);
				GL11.glVertex2f(x, y);
				GL11.glTexCoord2d(1, 0);
				GL11.glVertex2f(x + width, y);
				GL11.glTexCoord2d(1, 1);
				GL11.glVertex2f(x + width, y + height);
				GL11.glTexCoord2d(0, 1);
				GL11.glVertex2f(x, y + height);
			}
			GL11.glEnd();
		}
		GL11.glPopMatrix();
	}

	public void drawRect(int x, int y, float width, float height, Color color)
	{
		color.color();
		drawRect(x, y, width, height);
		Color.WHITE.color();
	}

	public int sizeofString(String text, int size)
	{
		return text.length() * (int)(size * (6.0f / 8.0f));
	}
}
