package fr.tmr.core.gui;

public interface IScreen
{
	public void render();

	public void update(int tick);
}
