package fr.tmr.core.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class Texture
{
	int width, height;
	int id;

	public Texture(String path)
	{
		loadTexture(path);
	}

	public void loadTexture(String path)
	{
		BufferedImage image = null;
		try
		{
			// affiche dans la console le chemin d'accès à la ressource
			System.out.println(Texture.class.getResource(path).getPath().replaceAll("%20", " "));
			// convertit le fichier en BufferedImage
			image = ImageIO.read(Texture.class.getResource(path));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		// Initialise les variables correspondantes à la largeur et la hauteur de l'image
		this.width = image.getWidth();
		this.height = image.getHeight();

		// Initialise un tableau contenant chaque pixel de l'image
		int[] pixels = new int[this.width * this.height];

		// Obtient le code RGBA de l'image
		image.getRGB(0, 0, this.width, this.height, pixels, 0, this.width);

		// Création d'un buffer
		ByteBuffer buffer = BufferUtils.createByteBuffer(this.width * this.height * 4);

		// Pour chaque pixel, mettre le code du rouge, du vert, du bleu, de l'alpha
		for(int y = 0; y < this.width; y++)
		{
			for(int x = 0; x < this.height; x++)
			{
				// Obtient le code RGBA du pixel
				int i = pixels[x + y * this.height];
				// obtient et insert le code du rouge dans le buffer depuis le code RGBA
				buffer.put((byte)((i >> 16) & 0xFF));
				// obtient et insert le code du vert dans le buffer depuis le code RGBA
				buffer.put((byte)((i >> 8) & 0xFF));
				// obtient et insert le code du bleu dans le buffer depuis le code RGBA
				buffer.put((byte)((i) & 0xFF));
				// obtient et insert le code de l'alpha dans le buffer depuis le code RGBA
				buffer.put((byte)((i >> 24) & 0xFF));
			}
		}

		buffer.flip();

		// Obtient l'id de la texture
		this.id = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.id);

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, this.width, this.height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

	}

	public int getWidth()
	{
		return this.width;
	}

	public int getHeight()
	{
		return this.height;
	}

	public void bind()
	{
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.id);
	}

	public void unbind()
	{
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
}
