package fr.tmr.core.graphics;

import org.lwjgl.opengl.GL11;

public class Camera
{
	private int[] bounds = new int[4];

	private float x, y;

	public Camera()
	{
		this(0, 0);
	}

	public Camera(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	// Set ne position of the camera
	public void changePosition(float x, float y)
	{
		if(y > this.bounds[0])
			y = this.bounds[0];
		if(y < this.bounds[2])
			y = this.bounds[2];
		if(x > this.bounds[1])
			x = bounds[1];
		if(x < this.bounds[3])
			x = this.bounds[3];

		this.x = x;
		this.y = y;
	}

	// Translate camera
	public void translate(float x, float y)
	{
		this.changePosition(this.x += x, this.y += y);
	}

	// Set limits
	public void setBounds(int bottom, int left, int top, int right)
	{
		this.bounds = new int[] {bottom, left, top, right};
	}

	// Set limits
	public void setBounds(int[] bounds)
	{
		this.bounds = bounds;
	}

	// Move camera
	public void updatePosition()
	{
		GL11.glTranslatef(x, y, 0);
	}

	// Move camera to origin
	public void resetPosition()
	{
		GL11.glTranslatef(-x, -y, 0);
	}

	public float[] getPosition()
	{
		return new float[] {this.x, this.y};
	}
}
