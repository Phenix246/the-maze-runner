package fr.tmr.core.graphics;

import org.lwjgl.opengl.GL11;

import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.utils.ResourcesLinks;

public class Renderer
{
	private static String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@ !\"#$%&'()*+,-./`:;<=>?[\\]^_{|}~";

	private static void quandRender(float x, float y, float width, float height, int xOffset, int yOffset, float textureWidth, float textureHeight)
	{
		// trace le carré en défénissant pour chaque coté la couleur.
		GL11.glTexCoord2f((0 + xOffset) / textureWidth, (0 + yOffset) / textureHeight);
		GL11.glVertex2f(x, y);
		GL11.glTexCoord2f((1 + xOffset) / textureWidth, (0 + yOffset) / textureHeight);
		GL11.glVertex2f(x + width, y);
		GL11.glTexCoord2f((1 + xOffset) / textureWidth, (1 + yOffset) / textureHeight);
		GL11.glVertex2f(x + width, y + height);
		GL11.glTexCoord2f((0 + xOffset) / textureWidth, (1 + yOffset) / textureHeight);
		GL11.glVertex2f(x, y + height);
	}

	public static void renderColor(float x, float y, float w, float h, float[] color)
	{
		// GL11.glPushMatrix();
		// {

		// Début du rendu du carré
		GL11.glBegin(GL11.GL_QUADS);
		{
			// On définie la couleur
			GL11.glColor3f(color[0], color[1], color[2]);

			// Renderer.quadData(x, y, w, h, color, 0, 0);

			// rendu de la couleur
			GL11.glVertex2f(x, y);
			GL11.glVertex2f(x + w, y);
			GL11.glVertex2f(x + w, y + h);
			GL11.glVertex2f(x, y + h);

			// On remet la couleur par défaut
			// GL11.glClearColor(color[0], color[1], color[2], color[3]);
		}
		// Fin du rendu du carré
		GL11.glEnd();
		// }
		// GL11.glPopMatrix();
	}

	public static void renderBlock(Texture texture, float x, float y, int offsetX, int offsetY)
	{
		GL11.glPushMatrix();
		{
			// Charge la texture
			texture.bind();
			// Début du rendu du carré
			GL11.glBegin(GL11.GL_QUADS);
			{
				// rendu de la texture
				quandRender(x * TheMazeRunner.Block_SCALE, y * TheMazeRunner.Block_SCALE, TheMazeRunner.Block_SCALE, TheMazeRunner.Block_SCALE, offsetX, offsetY, 8.0F, 8.0F);
			}
			// Fin du rendu du carré
			GL11.glEnd();
			// Décharge la texture
			texture.unbind();
		}
		GL11.glPopMatrix();
	}

	public static void renderEntity(Texture texture, float x, float y, int offsetX, int offsetY)
	{
		GL11.glPushMatrix();
		{
			// Charge la texture
			texture.bind();
			// Début du rendu du carré
			GL11.glBegin(GL11.GL_QUADS);
			{
				// rendu de la texture
				Renderer.quandRender(x, y, TheMazeRunner.ENTITY_SCALE, TheMazeRunner.ENTITY_SCALE, offsetX, offsetY, texture.getWidth() / texture.getHeight(), 1);
			}
			// Fin du rendu du carré
			GL11.glEnd();
			// Décharge la texture
			texture.unbind();
		}
		GL11.glPopMatrix();
	}

	public static void renderTexture(Texture texture, float x, float y, float width, float height, int offsetX, int offsetY)
	{
		// GL11.glPushMatrix();
		// {
		// Charge la texture
		texture.bind();
		// Début du rendu du carré
		GL11.glBegin(GL11.GL_QUADS);
		{
			// rendu de la texture

			GL11.glTexCoord2f((0 + offsetX), (0 + offsetY));
			GL11.glVertex2f(x, y);
			GL11.glTexCoord2f((1 + offsetX), (0 + offsetY));
			GL11.glVertex2f(x + width, y);
			GL11.glTexCoord2f((1 + offsetX), (1 + offsetY));
			GL11.glVertex2f(x + width, y + height);
			GL11.glTexCoord2f((0 + offsetX), (1 + offsetY));
			GL11.glVertex2f(x, y + height);
			// Renderer.quadData(x, y, width, height, color, offsetX, offsetY);
		}
		// Fin du rendu du carré
		GL11.glEnd();
		// Décharge la texture
		texture.unbind();
		// }
		// GL11.glPopMatrix();
	}

	// Méthodes pour le rendu des chaines de caractaires

	public static void drawString(String text, int x, int y, int size)
	{
		GL11.glPushMatrix();
		{
			ResourcesLinks.font.bind();
			GL11.glColor4f(1, 1, 1, 1);
			GL11.glBegin(GL11.GL_QUADS);
			{
				for(int i = 0; i < text.length(); i++)
				{
					drawChar(text.charAt(i), x + i * (int)(size * (6.0f / 8.0f)), y, size);
				}
			}
			GL11.glEnd();
			ResourcesLinks.font.unbind();
		}
		GL11.glPopMatrix();
		Renderer.color(1, 1, 1, 1);
	}

	public static void drawString(String text, int x, int y, int size, boolean center)
	{
		GL11.glPushMatrix();
		{
			if(center)
				x -= (text.length() * size) / 2;
			ResourcesLinks.font.bind();
			GL11.glColor4f(1, 1, 1, 1);
			GL11.glBegin(GL11.GL_QUADS);
			{
				for(int i = 0; i < text.length(); i++)
				{
					drawChar(text.charAt(i), x + i * (int)(size * (6.0f / 8.0f)), y, size);
				}
			}
			GL11.glEnd();
			ResourcesLinks.font.unbind();
		}
		GL11.glPopMatrix();
		Renderer.color(1, 1, 1, 1);
	}

	public static void drawString(String text, int x, int y, int size, boolean center, boolean colored)
	{

		GL11.glPushMatrix();
		{
			if(center)
				x -= (text.length() * size) / 2;
			ResourcesLinks.font.bind();
			if(!colored)
				GL11.glColor4f(1, 1, 1, 1);
			GL11.glBegin(GL11.GL_QUADS);
			{
				for(int i = 0; i < text.length(); i++)
				{
					drawChar(text.charAt(i), x + i * (int)(size * (6.0f / 8.0f)), y, size);
				}
			}
			GL11.glEnd();
			ResourcesLinks.font.unbind();
		}
		GL11.glPopMatrix();
		Renderer.color(1, 1, 1, 1);

	}

	public static void drawChar(char character, int x, int y, int size)
	{
		int i = chars.indexOf(character);
		int xo = i % 16;
		int yo = i / 16;

		GL11.glTexCoord2f((0 + xo) / 16.0f, (0 + yo) / 16.0f);
		GL11.glVertex2f(x, y);
		GL11.glTexCoord2f((1 + xo) / 16.0f, (0 + yo) / 16.0f);
		GL11.glVertex2f(x + size, y);
		GL11.glTexCoord2f((1 + xo) / 16.0f, (1 + yo) / 16.0f);
		GL11.glVertex2f(x + size, y + size);
		GL11.glTexCoord2f((0 + xo) / 16.0f, (1 + yo) / 16.0f);
		GL11.glVertex2f(x, y + size);
	}

	public static void color(float red, float green, float blue, float alpha)
	{
		GL11.glColor4f(red, green, blue, alpha);
	}
}
