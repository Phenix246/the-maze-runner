package fr.tmr.core.graphics;

import fr.tmr.core.time.Delay;

public class Animation
{
	private int frameNumber;
	private int currentFrame;
	private int frameSize;
	private Texture texture;
	private Delay delay;

	public Animation(Texture texture, int delay)
	{
		this.texture = texture;
		this.frameNumber = texture.getWidth() / texture.getHeight();
		this.currentFrame = 0;
		this.frameSize = texture.getHeight();
		this.delay = new Delay(delay);
	}

	public void render(float x, float y)
	{
		Renderer.renderEntity(this.texture, x, y, this.currentFrame, 0);
		if(this.delay.isOver())
		{
			this.currentFrame++;
			if(this.currentFrame >= this.frameNumber)
				this.currentFrame = 0;
			this.delay.restart();
		}
	}

	public Animation start()
	{
		this.delay.start();
		return this;
	}

	public Animation stop()
	{
		this.delay.stop();
		return this;
	}
}