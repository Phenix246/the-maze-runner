package fr.tmr.core;

public abstract class Game
{
	public abstract void init();

	public abstract void render();

	public abstract void update(int tick);

	public abstract void close();
}
