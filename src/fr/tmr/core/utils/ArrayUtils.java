package fr.tmr.core.utils;

public class ArrayUtils
{
	private static int getSize(Object[]... array)
	{
		int size = 0;
		for(int i = 0; i < array.length; i++)
			size += array[i].length;
		return size;
	}

	public static float[] fusion(float[]... array)
	{
		float[] na = new float[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static double[] fusion(double[]... array)
	{
		double[] na = new double[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static boolean[] fusion(boolean[]... array)
	{
		boolean[] na = new boolean[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static byte[] fusion(byte[]... array)
	{
		byte[] na = new byte[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static short[] fusion(short[]... array)
	{
		short[] na = new short[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static int[] fusion(int[]... array)
	{
		int[] na = new int[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static long[] fusion(long[]... array)
	{
		long[] na = new long[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

	public static Object[] fusion(Object[]... array)
	{
		Object[] na = new Object[getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
		{
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		}
		return na;
	}

}
