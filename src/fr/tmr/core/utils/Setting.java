package fr.tmr.core.utils;

import java.util.ArrayList;
import java.util.List;

public class Setting
{
	private ArrayList<String> values;
	private String name;

	public Setting()
	{
		this("Untitled");
	}

	public Setting(String name)
	{
		values = new ArrayList<String>();
		this.name = name;
	}

	public void addSection(String name)
	{
		values.add("\n" + "- " + name);
	}

	public Setting set(String string)
	{
		String[] lines = string.split("\n");
		this.name = lines[1].split(" - ")[1];
		for(int i = 0; i < lines.length; i++)
		{
			if(!lines[i].startsWith("/"))
				values.add(lines[i]);
		}
		return this;
	}

	public void add(String name, int value)
	{
		values.add(name + "/i/" + value);
	}

	public void add(String name, float value)
	{
		values.add(name + "/f/" + value);
	}

	public void add(String name, String value)
	{
		values.add(name + "/s/" + value);
	}

	@Override
	public String toString()
	{
		String result = "/---------------------------------\n" + "/Setting - " + name + "\n" + "/---------------------------------\n";

		for(int i = 0; i < values.size(); i++)
		{
			result += values.get(i);
			if(i != values.size() - 1)
			{
				result += "\n";
			}
		}

		if(values.isEmpty())
		{
			result += "( Empty )";
		}

		return result;
	}

	// GETTERS & SETTERS

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<String> getValues()
	{
		return values;
	}

	// Getters

	public int getInteger(String name)
	{
		for(int i = 0; i < values.size(); i++)
		{
			String[] token = values.get(i).split("/");
			if(token.length == 3 && token[1].equals("i") && token[0].equals(name))
				return Integer.parseInt(token[2]);
		}
		return 0;
	}

	public float getFloat(String name)
	{
		for(int i = 0; i < values.size(); i++)
		{
			String[] token = values.get(i).split("/");
			if(token.length == 3 && token[1].equals("f") && token[0].equals(name))
				return Float.parseFloat(token[2]);
		}
		return 0;
	}

	public String getString(String name)
	{
		for(int i = 0; i < values.size(); i++)
		{
			String[] token = values.get(i).split("/");
			if(token.length == 3 && token[1].equals("s") && token[0].equals(name))
				return token[2];
		}
		return "null";
	}

	// Setters

	public void setInteger(String name, int value)
	{
		for(int i = 0; i < values.size(); i++)
		{
			String[] token = values.get(i).split("/");
			if(token.length == 3 && token[1].equals("i") && token[0].equals(name))
				values.set(i, name + "/i/" + value);
		}
	}

	public void setFloat(String name, float value)
	{
		for(int i = 0; i < values.size(); i++)
		{
			String[] token = values.get(i).split("/");
			if(token.length == 3 && token[1].equals("f") && token[0].equals(name))
				values.set(i, name + "/f/" + value);
		}
	}

	public void setString(String name, String value)
	{
		for(int i = 0; i < values.size(); i++)
		{
			String[] token = values.get(i).split("/");
			if(token.length == 3 && token[1].equals("s") && token[0].equals(name))
				values.set(i, name + "/s/" + value);
		}
	}

}
