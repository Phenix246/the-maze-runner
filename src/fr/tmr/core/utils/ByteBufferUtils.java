package fr.tmr.core.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

public class ByteBufferUtils
{
	// Charge une image et la transforme en Byte
	public static ByteBuffer createIcon(String path)
	{
		BufferedImage image = null;
		try
		{
			// affiche dans la console le chemin d'accès à la ressource
			System.out.println(ByteBufferUtils.class.getResource(path).getPath().replaceAll("%20", " "));
			// convertit le fichier en BufferedImage
			image = ImageIO.read(ByteBufferUtils.class.getResource(path));

			return imageToByteBuffer(image);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	// Transforme une image en Byte
	private static ByteBuffer imageToByteBuffer(BufferedImage image)
	{
		if(image.getType() != BufferedImage.TYPE_INT_ARGB_PRE)
		{
			BufferedImage convertedImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB_PRE);
			Graphics2D g = convertedImage.createGraphics();
			double width = image.getWidth() * (double)1;
			double height = image.getHeight() * (double)1;
			g.drawImage(image, (int)((convertedImage.getWidth() - width) / 2), (int)((convertedImage.getHeight() - height) / 2), (int)(width), (int)(height), null);
			g.dispose();
			image = convertedImage;
		}

		// Creéation d'un tableau contenant 4 x la largeur de l'image x la hauteur de l'image
		// emplacement de type octet
		byte[] imageBuffer = new byte[image.getWidth() * image.getHeight() * 4];
		int counter = 0;
		// Pour chaque pixel de l'image
		for(int i = 0; i < image.getHeight(); i++)
		{
			for(int j = 0; j < image.getWidth(); j++)
			{
				// obtient le code RGBA (Red, Green, Blue, Alpha)
				int colorSpace = image.getRGB(j, i);
				// obtient et insert le code du rouge dans le tableau d'octet depuis le code RGBA
				imageBuffer[counter + 0] = (byte)((colorSpace << 8) >> 24);
				// obtient et insert le code du vert dans le tableau d'octet depuis le code RGBA
				imageBuffer[counter + 1] = (byte)((colorSpace << 16) >> 24);
				// obtient et insert le code du bleu dans le tableau d'octet depuis le code RGBA
				imageBuffer[counter + 2] = (byte)((colorSpace << 24) >> 24);
				// obtient et insert le code de l'alpha dans le tableau d'octet depuis le code RGBA
				imageBuffer[counter + 3] = (byte)(colorSpace >> 24);
				counter += 4;
			}
		}
		// On retourne le buffer
		return ByteBuffer.wrap(imageBuffer);
	}
}
