package fr.tmr.core.utils;

import fr.tmr.game.TheMazeRunner;

public abstract class GameObject
{
	public static final int DEFAULT_ID = 0;
	public static final int ITEM_ID = 1;
	public static final int PLAYER_ID = 2;
	public static final int ENEMY_ID = 3;

	public float x;
	public float y;
	protected int type;

	protected boolean[] flags = new boolean[1];

	public GameObject(int type)
	{
		this.type = type;
	}

	public void update(int ticks)
	{}

	public void render()
	{}

	public int getType()
	{
		return type;
	}

	public boolean isRemove()
	{
		return flags[0];
	}

	public void remove()
	{
		flags[0] = true;
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public GameObject init(float x, float y)
	{
		this.x = x * TheMazeRunner.Block_SCALE;
		this.y = y * TheMazeRunner.Block_SCALE;
		return this;
	}
}