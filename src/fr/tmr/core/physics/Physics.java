package fr.tmr.core.physics;

//import java.awt.Rectangle;
import java.util.ArrayList;

import fr.tmr.core.maths.MathsHelper;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.block.Block;
import fr.tmr.game.entity.Entity;

public class Physics
{
	// variable pour définir si la physique est activée
	private static boolean activePhysics = true;

	public static boolean isPhysics()
	{
		return activePhysics;
	}

	// Entity collide

	// Retourne un tableau contenant toutes les entitées se trouvant à un certin rayon de l'entitée
	public static ArrayList<Entity> sphereCollide(float x, float y, float radius)
	{
		// Initialise un tableau d'entitée
		ArrayList<Entity> list = new ArrayList<Entity>();

		// Pour chaque entitée, on teste la distance entre les deux entitées
		for(Entity go : TheMazeRunner.instance.level.entitiesList)
		{
			// Si la distance est inférieur aux rayon donné on ajoute l'entité au tableau
			if(MathsHelper.distance2d(go.getX(), go.getY(), x, y) < radius * TheMazeRunner.Block_SCALE)
				list.add(go);
		}

		// Enfin on retourne le tableau
		return list;
	}

	// public static ArrayList<Entity> rectangleCollide(float x1, float y1,
	// float x2, float y2)
	// {
	// ArrayList<Entity> list = new ArrayList<Entity>();
	//
	// Rectangle collider = new Rectangle((int)(x1 * 100), (int)(y1 * 100),
	// (int)((x2 - x1) * 100), (int)((y2 - y1) * 100));
	//
	// for(Entity go : TheMazeRunner.instance.level.entitiesList)
	// {
	// if(Physics.checkCollision(collider, go) != null)
	// list.add(go);
	// }
	// return list;
	// }
	//
	// public static GameObject checkCollision(Entity go1, Entity go2)
	// {
	// return checkCollision(new Rectangle((int)(go1.getBlockX() * 100),
	// (int)(go1.getBlockY() * 100), (int)(go1.getSize() * 100),
	// (int)(go1.getSize() * 100)), go2);
	// }
	//
	// public static GameObject checkCollision(Rectangle r1, Entity go2)
	// {
	// if(r1.intersects(new Rectangle((int)(go2.getBlockX() * 100),
	// (int)(go2.getBlockY() * 100), (int)(go2.getSize() * 100),
	// (int)(go2.getSize() * 100))))
	// return go2;
	// else
	// return null;
	// }

	// Block collide

	// test si l'entitée rencontre un bloc suivant un déplacement sur l'axe x
	public static boolean isBlockCollideX(Entity entity, float x)
	{
		// test si la physic est activé
		if(!activePhysics)
			return false;

		Block block1, block2;

		// test de la valeur de x afin de tenir en compte ou non de la largeur de l'entité
		if(x < 0)
		{
			block1 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(x), (int)entity.getBlockY());
			block2 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(x), (int)entity.getBlockY(TheMazeRunner.ENTITY_SCALE));
		}
		else
		{
			block1 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(x + TheMazeRunner.ENTITY_SCALE), (int)entity.getBlockY());
			block2 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(x + TheMazeRunner.ENTITY_SCALE), (int)entity.getBlockY(TheMazeRunner.ENTITY_SCALE));
		}
		// test si le block1 est le même que le block2 puis test si il est solide
		if(block1.equals(block2))
		{
			if(block1.isSolid())
				return true;
		}
		// sinon test si l'un des blocs est solide
		else if(block1.isSolid() || block2.isSolid())
			return true;
		return false;
	}

	// test si l'entitée rencontre un bloc suivant un déplacement sur l'axe y
	public static boolean isBlockCollideY(Entity entity, float y)
	{
		// test si la physic est activé
		if(!activePhysics)
			return false;

		Block block1, block2;

		// test de la valeur de y afin de tenir en compte ou non de la hauteur de l'entité
		if(y < 0)
		{
			block1 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(), (int)entity.getBlockY(y));
			block2 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(TheMazeRunner.ENTITY_SCALE), (int)entity.getBlockY(y));
		}
		else
		{
			block1 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(), (int)entity.getBlockY(y + TheMazeRunner.ENTITY_SCALE));
			block2 = TheMazeRunner.instance.level.getBlock((int)entity.getBlockX(TheMazeRunner.ENTITY_SCALE), (int)entity.getBlockY(y + TheMazeRunner.ENTITY_SCALE));
		}
		// test si le block1 est le même que le block2 puis test si il est solide
		if(block1.equals(block2))
		{
			if(block1.isSolid())
				return true;
		}
		// sinon test si l'un des blocs est solide
		else if(block1.isSolid() || block2.isSolid())
			return true;
		return false;
	}
}