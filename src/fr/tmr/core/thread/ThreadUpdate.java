package fr.tmr.core.thread;

import fr.tmr.core.Engine;

public class ThreadUpdate extends Thread
{

	// public ThreadUpdate()
	// {}

	public ThreadUpdate(String name)
	{
		super(name);
	}

	@Override
	public void run()
	{
		// timer variables
		long timer = System.currentTimeMillis();
		long lastTime = System.nanoTime();

		double nanoSeconds = 1_000_000_000.0D / 60.0D;
		double passed = 0;

		int ticks = 0;

		// Calcul le temps écouler entre deux opérations
		long now = System.nanoTime();
		passed = now - lastTime;
		lastTime = now;

		// Si le temps passer est supérieur à nanoSeconds
		if(passed > nanoSeconds)
		{
			lastTime += nanoSeconds;

			// On update le jeu
			Engine.game.update(ticks);

			// Ajoute 1 au nombre de ticks
			ticks++;
		}

		// Chaque seconde afficher le nombre de ticks
		if(System.currentTimeMillis() - timer > 1_000)
		{
			timer += 1_000;

			// on change le nombre de ticks pour ce cycle
			Engine.ticks = ticks;

			// Remet à zéro le nombre de ticks
			ticks = 0;
		}

	}
}
