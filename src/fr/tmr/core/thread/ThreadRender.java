package fr.tmr.core.thread;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import fr.tmr.core.Engine;

public class ThreadRender extends Thread
{

	// public ThreadRender()
	// {}

	public ThreadRender(String name)
	{
		super(name);
	}

	@Override
	public void run()
	{
		// timer variables
		long timer = System.currentTimeMillis();
		long lastTime = System.nanoTime();

		double nanoSeconds = 1_000_000_000.0D / 60.0D;
		double passed = 0;

		int frames = 0;

		// Calcul le temps écouler entre deux opérations
		long now = System.nanoTime();
		passed = now - lastTime;
		lastTime = now;

		// Update l'écran
		Display.update();

		// Si on désir fermer la fenêtre, définir la variable isRunning comme fausse
		if(Display.isCloseRequested())
		{
			Engine.game.close();
			Engine.isRunning = false;
		}

		// Si le temps passer est supérieur à nanoSeconds
		if(passed > nanoSeconds)
		{
			lastTime += nanoSeconds;
		}
		else
		{
			// Définie les nombre maximum de rendu par seconde
			Display.sync(Engine.fpsCap);

			// Obtient la nouvelle largeur de l'écran
			Engine.width = Display.getWidth();

			// Obtient la nouvelle heuteur de l'écran
			Engine.height = Display.getHeight();

			// Met à jour les informations de Open GL
			Engine.view2D(Engine.width, Engine.height);

			// Efface le contenu de l'écran
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

			// Rend le jeu
			Engine.game.render();

			// Ajoute 1 au nombre de ticks
			frames++;
		}

		// Chaque seconde afficher le nombre de ticks
		if(System.currentTimeMillis() - timer > 1_000)
		{
			timer += 1_000;

			// on change le nombre de frames pour ce cycle
			Engine.frames = frames;

			// Remet à zéro le nombre de frames
			frames = 0;
		}
	}

}
