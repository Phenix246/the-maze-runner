package fr.tmr.core.maths;

public class Vector3f
{
	public static final Vector3f ZERO = new Vector3f();

	private float x, y, z;

	public Vector3f()
	{
		this(0, 0, 0);
	}

	public Vector3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3f(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	public Vector3f set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;

		return this;
	}

	public Vector3f set(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();

		return this;
	}

	// Méthodes utiles

	public float sqrt()
	{
		return x * x + y * y + z * z;
	}

	public float length()
	{
		return (float)Math.sqrt(sqrt());
	}

	public Vector3f normalize()
	{
		x /= length();
		y /= length();
		z /= length();

		return this;
	}

	public float dot(Vector3f vec)
	{
		return x * vec.getX() + y * vec.getY() + z * vec.getZ();
	}

	@Override
	public String toString()
	{
		return x + " " + y + " " + z;
	}

	public String toStringDetailed()
	{
		return "(x: " + x + " ,y: " + y + " ,z: " + z + ")";
	}

	public Vector3f negate()
	{
		x = -x;
		y = -y;
		z = -z;

		return this;
	}

	// Opérations globales

	// Addition
	public Vector3f add(Vector3f vec)
	{
		x += vec.getX();
		y += vec.getY();
		z += vec.getZ();

		return this;
	}

	public Vector3f add(float value)
	{
		x += value;
		y += value;
		z += value;

		return this;
	}

	// Soustraction
	public Vector3f sub(Vector3f vec)
	{
		x -= vec.getX();
		y -= vec.getY();
		z -= vec.getZ();

		return this;
	}

	public Vector3f sub(float value)
	{
		x -= value;
		y -= value;
		z -= value;

		return this;
	}

	// Multiplication
	public Vector3f mul(Vector3f vec)
	{
		x *= vec.getX();
		y *= vec.getY();
		z *= vec.getZ();

		return this;
	}

	public Vector3f mul(float value)
	{
		x *= value;
		y *= value;
		z *= value;

		return this;
	}

	// Division
	public Vector3f div(Vector3f vec)
	{
		x /= vec.getX();
		y /= vec.getY();
		z /= vec.getZ();

		return this;
	}

	public Vector3f div(float value)
	{
		x /= value;
		y /= value;
		z /= value;

		return this;
	}

	// Opération en X
	public Vector3f addX(float value)
	{
		x += value;

		return this;
	}

	public Vector3f subX(float value)
	{
		x -= value;

		return this;
	}

	public Vector3f mulX(float value)
	{
		x *= value;

		return this;
	}

	public Vector3f divX(float value)
	{
		x /= value;

		return this;
	}

	// Opération en Y
	public Vector3f addY(float value)
	{
		y += value;

		return this;
	}

	public Vector3f subY(float value)
	{
		y -= value;

		return this;
	}

	public Vector3f mulY(float value)
	{
		y *= value;

		return this;
	}

	public Vector3f divY(float value)
	{
		y /= value;

		return this;
	}

	// Opération en Z
	public Vector3f addZ(float value)
	{
		z += value;

		return this;
	}

	public Vector3f subZ(float value)
	{
		z -= value;

		return this;
	}

	public Vector3f mulZ(float value)
	{
		z *= value;

		return this;
	}

	public Vector3f divZ(float value)
	{
		z /= value;

		return this;
	}

	// Getters and setters

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}
}
