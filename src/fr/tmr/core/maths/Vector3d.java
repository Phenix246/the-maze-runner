package fr.tmr.core.maths;

public class Vector3d
{
	public static final Vector3d ZERO = new Vector3d();

	private double x, y, z;

	public Vector3d()
	{
		this(0, 0, 0);
	}

	public Vector3d(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3d(Vector3d vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	public Vector3d set(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;

		return this;
	}

	public Vector3d set(Vector3d vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();

		return this;
	}

	// Méthodes utiles

	public double sqrt()
	{
		return x * x + y * y + z * z;
	}

	public double length()
	{
		return Math.sqrt(sqrt());
	}

	public Vector3d normalize()
	{
		x /= length();
		y /= length();
		z /= length();

		return this;
	}

	public double dot(Vector3d vec)
	{
		return x * vec.getX() + y * vec.getY() + z * vec.getZ();
	}

	@Override
	public String toString()
	{
		return x + " " + y + " " + z;
	}

	public String toStringDetailed()
	{
		return "(x: " + x + " ,y: " + y + " ,z: " + z + ")";
	}

	public Vector3d negate()
	{
		x = -x;
		y = -y;
		z = -z;

		return this;
	}

	// Opérations globales

	// Addition
	public Vector3d add(Vector3d vec)
	{
		x += vec.getX();
		y += vec.getY();
		z += vec.getZ();

		return this;
	}

	public Vector3d add(double value)
	{
		x += value;
		y += value;
		z += value;

		return this;
	}

	// Soustraction
	public Vector3d sub(Vector3d vec)
	{
		x -= vec.getX();
		y -= vec.getY();
		z -= vec.getZ();

		return this;
	}

	public Vector3d sub(double value)
	{
		x -= value;
		y -= value;
		z -= value;

		return this;
	}

	// Multiplication
	public Vector3d mul(Vector3d vec)
	{
		x *= vec.getX();
		y *= vec.getY();
		z *= vec.getZ();

		return this;
	}

	public Vector3d mul(double value)
	{
		x *= value;
		y *= value;
		z *= value;

		return this;
	}

	// Division
	public Vector3d div(Vector3d vec)
	{
		x /= vec.getX();
		y /= vec.getY();
		z /= vec.getZ();

		return this;
	}

	public Vector3d div(double value)
	{
		x /= value;
		y /= value;
		z /= value;

		return this;
	}

	// Opération en X
	public Vector3d addX(double value)
	{
		x += value;

		return this;
	}

	public Vector3d subX(double value)
	{
		x -= value;

		return this;
	}

	public Vector3d mulX(double value)
	{
		x *= value;

		return this;
	}

	public Vector3d divX(double value)
	{
		x /= value;

		return this;
	}

	// Opération en Y
	public Vector3d addY(double value)
	{
		y += value;

		return this;
	}

	public Vector3d subY(double value)
	{
		y -= value;

		return this;
	}

	public Vector3d mulY(double value)
	{
		y *= value;

		return this;
	}

	public Vector3d divY(double value)
	{
		y /= value;

		return this;
	}

	// Opération en Z
	public Vector3d addZ(double value)
	{
		z += value;

		return this;
	}

	public Vector3d subZ(double value)
	{
		z -= value;

		return this;
	}

	public Vector3d mulZ(double value)
	{
		z *= value;

		return this;
	}

	public Vector3d divZ(double value)
	{
		z /= value;

		return this;
	}

	// Getters and setters

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getZ()
	{
		return z;
	}

	public void setZ(double z)
	{
		this.z = z;
	}
}
