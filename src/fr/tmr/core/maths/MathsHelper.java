package fr.tmr.core.maths;

public class MathsHelper
{
	public static float distance2d(double x1, double y1, double x2, double y2)
	{
		double x = x2 - x1;
		double y = y2 - y1;
		return (float)Math.sqrt((x * x) + (y * y));
	}

	public static float distance3d(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		double x = x2 - x1;
		double y = y2 - y1;
		double z = z2 - z1;
		return (float)Math.sqrt((x * x) + (y * y) + (z * z));
	}

	public static boolean betweenS(double x1, double x2, double xCompare)
	{
		if(x1 < xCompare && xCompare < x2)
			return true;
		return false;
	}

	public static boolean between(double x1, double x2, double xCompare)
	{
		if(x1 <= xCompare && xCompare <= x2)
			return true;
		return false;
	}
}