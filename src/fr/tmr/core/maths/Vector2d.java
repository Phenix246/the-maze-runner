package fr.tmr.core.maths;

public class Vector2d
{

	public static final Vector2d ZERO = new Vector2d();

	private double x, y;

	public Vector2d()
	{
		this(0, 0);
	}

	public Vector2d(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2d(Vector2d vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
	}

	public Vector2d set(double x, double y)
	{
		this.x = x;
		this.y = y;

		return this;
	}

	public Vector2d set(Vector2d vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();

		return this;
	}

	// Méthodes utiles

	public double sqrt()
	{
		return x * x + y * y;
	}

	public double length()
	{
		return Math.sqrt(sqrt());
	}

	public Vector2d normalize()
	{
		x /= length();
		y /= length();

		return this;
	}

	public double dot(Vector2d vec)
	{
		return x * vec.getX() + y * vec.getY();
	}

	@Override
	public String toString()
	{
		return x + " " + y;
	}

	public String toStringDetailed()
	{
		return "(x: " + x + " ,y: " + y + ")";
	}

	public Vector2d negate()
	{
		x = -x;
		y = -y;

		return this;
	}

	// Opérations globales

	// Addition
	public Vector2d add(Vector2d vec)
	{
		x += vec.getX();
		y += vec.getY();

		return this;
	}

	public Vector2d add(double value)
	{
		x += value;
		y += value;

		return this;
	}

	// Soustractions
	public Vector2d sub(Vector2d vec)
	{
		x -= vec.getX();
		y -= vec.getY();

		return this;
	}

	public Vector2d sub(double value)
	{
		x -= value;
		y -= value;

		return this;
	}

	// Multiplication
	public Vector2d mul(Vector2d vec)
	{
		x *= vec.getX();
		y *= vec.getY();

		return this;
	}

	public Vector2d mul(double value)
	{
		x *= value;
		y *= value;

		return this;
	}

	// Division
	public Vector2d div(Vector2d vec)
	{
		x /= vec.getX();
		y /= vec.getY();

		return this;
	}

	public Vector2d div(double value)
	{
		x /= value;
		y /= value;

		return this;
	}

	// Opération en X
	public Vector2d addX(double value)
	{
		x += value;

		return this;
	}

	public Vector2d subX(double value)
	{
		x -= value;

		return this;
	}

	public Vector2d mulX(double value)
	{
		x *= value;

		return this;
	}

	public Vector2d divX(double value)
	{
		x /= value;

		return this;
	}

	// Opération en Y
	public Vector2d addY(double value)
	{
		y += value;

		return this;
	}

	public Vector2d subY(double value)
	{
		y -= value;

		return this;
	}

	public Vector2d mulY(double value)
	{
		y *= value;

		return this;
	}

	public Vector2d divY(double value)
	{
		y /= value;

		return this;
	}

	// Getters and setters

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

}
