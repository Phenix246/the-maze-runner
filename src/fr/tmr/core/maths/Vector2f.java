package fr.tmr.core.maths;

public class Vector2f
{

	public static final Vector2f ZERO = new Vector2f();

	private float x, y;

	public Vector2f()
	{
		this(0, 0);
	}

	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2f(Vector2f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
	}

	public Vector2f set(float x, float y)
	{
		this.x = x;
		this.y = y;

		return this;
	}

	public Vector2f set(Vector2f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();

		return this;
	}

	// Méthodes utiles

	public float sqrt()
	{
		return x * x + y * y;
	}

	public float length()
	{
		return (float)Math.sqrt(sqrt());
	}

	public Vector2f normalize()
	{
		x /= length();
		y /= length();

		return this;
	}

	public float dot(Vector2f vec)
	{
		return x * vec.getX() + y * vec.getY();
	}

	@Override
	public String toString()
	{
		return x + " " + y;
	}

	public String toStringDetailed()
	{
		return "(x: " + x + " ,y: " + y + ")";
	}

	public Vector2f negate()
	{
		x = -x;
		y = -y;

		return this;
	}

	// Opérations globales

	// Addition
	public Vector2f add(Vector2f vec)
	{
		x += vec.getX();
		y += vec.getY();

		return this;
	}

	public Vector2f add(float value)
	{
		x += value;
		y += value;

		return this;
	}

	// Soustractions
	public Vector2f sub(Vector2f vec)
	{
		x -= vec.getX();
		y -= vec.getY();

		return this;
	}

	public Vector2f sub(float value)
	{
		x -= value;
		y -= value;

		return this;
	}

	// Multiplication
	public Vector2f mul(Vector2f vec)
	{
		x *= vec.getX();
		y *= vec.getY();

		return this;
	}

	public Vector2f mul(float value)
	{
		x *= value;
		y *= value;

		return this;
	}

	// Division
	public Vector2f div(Vector2f vec)
	{
		x /= vec.getX();
		y /= vec.getY();

		return this;
	}

	public Vector2f div(float value)
	{
		x /= value;
		y /= value;

		return this;
	}

	// Opération en X
	public Vector2f addX(float value)
	{
		x += value;

		return this;
	}

	public Vector2f subX(float value)
	{
		x -= value;

		return this;
	}

	public Vector2f mulX(float value)
	{
		x *= value;

		return this;
	}

	public Vector2f divX(float value)
	{
		x /= value;

		return this;
	}

	// Opération en Y
	public Vector2f addY(float value)
	{
		y += value;

		return this;
	}

	public Vector2f subY(float value)
	{
		y -= value;

		return this;
	}

	public Vector2f mulY(float value)
	{
		y *= value;

		return this;
	}

	public Vector2f divY(float value)
	{
		y /= value;

		return this;
	}

	// Getters and setters

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

}
