package fr.tmr.game.pathfinder;

public class Node
{
	public int x, y;

	public Node(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Node(Node node)
	{
		this(node.x, node.y);
	}

	public boolean equals(Node node)
	{
		return (node.x == this.x && node.y == this.y) ? true : false;
	}
}
