package fr.tmr.game.pathfinder;

public class Path extends Node
{
	public Path parent;
	public int value;

	public Path(int x, int y, Path parent, int value)
	{
		super(x, y);
		this.parent = parent;
		this.value = value;
	}

	public Path(Path path)
	{
		this(path.x, path.y, path.parent, path.value);
	}

	public Path(Node node, Path parent, int value)
	{
		this(node.x, node.y, parent, value);
	}
}
