package fr.tmr.game.pathfinder;

import java.util.ArrayList;

import fr.tmr.core.physics.Physics;
import fr.tmr.game.level.Level;

/**
 * Pathfinder dit de la distance de Manhattan
 */
public class PathfinderManhattan
{
	private Level level;
	private ArrayList<Path> blacklist;
	private ArrayList<Node> path;

	public PathfinderManhattan(Level level)
	{
		this.level = level;
		this.blacklist = new ArrayList<Path>();
		this.path = null;
	}

	public void find(Node start, Node end)
	{
		// On reinnitialize les variables
		this.path = null;
		this.blacklist.clear();

		int index = 0;
		Path current;

		// On définie le nombre de pas maximum
		int maxSize = ((this.level.worldLimitX) * (this.level.worldLimitY));

		// On ajoute le point de début à la blacklist
		this.blacklist.add(new Path(start.x, start.y, null, 0));

		do
		{
			// On vérifie la taille de l'index et de la blacklist afin d'éviter un
			// outofboundException
			if(index >= this.blacklist.size())
				return;

			// On définie le dernier point de passage comme étant celui avec lequel on va travailler
			current = this.blacklist.get(index);

			// On vérifie si on a atteint notre objectif
			if(current.equals(end))
				break;

			// On teste pour savoir si on peut aller dans les 4 directions,
			// on les ajoute à la blacklist si c'est la cas
			if(isWalking(new Node(current.x + 1, current.y)))
				this.blacklist.add(new Path(current.x + 1, current.y, current, current.value++));
			if(isWalking(new Node(current.x, current.y + 1)))
				this.blacklist.add(new Path(current.x, current.y + 1, current, current.value++));
			if(isWalking(new Node(current.x - 1, current.y)))
				this.blacklist.add(new Path(current.x - 1, current.y, current, current.value++));
			if(isWalking(new Node(current.x, current.y - 1)))
				this.blacklist.add(new Path(current.x, current.y - 1, current, current.value++));

			// On incrémente l'index
			index++;
		}
		while(this.blacklist.size() < maxSize);

		// On initialize un nouveau chemin
		this.path = new ArrayList<Node>();

		// On récupère le chemin final
		Path next = new Path(current);

		// On remet dans l'ordre les nodes du début jusqu'a la fin
		while(next != null)
		{
			this.path.add(0, new Node(next));
			if(next.parent != null)
			{
				next = new Path(next.parent);
			}
			else
			{
				next = null;
			}
		}
	}

	// On teste la node pour savoir si on peut l'enprunter
	private boolean isWalking(Node node)
	{
		// On teste si la node est en dehors des limites de la cartes
		if(node.x <= 0 || node.y <= 0 || node.x >= this.level.worldLimitX || node.y >= this.level.worldLimitY)
			return false;

		// On vérifie si la node n'est pas déjà présente dans le chemin
		if(inList(node))
			return false;

		// On teste si on peut empunter le chemin
		if(Physics.isPhysics()) // TODO: fix
			return !this.level.getBlock(node.x, node.y).isSolid();
		return true;
	}

	// On regarde si la node est dans la liste
	private boolean inList(Node node)
	{
		for(int i = 0; i < this.blacklist.size(); i++)
		{
			if(this.blacklist.get(i).equals(node))
				return true;
		}
		return false;
	}

	// On retourne le chemin
	public ArrayList<Node> getPath()
	{
		return this.path;
	}

	public boolean hasPath()
	{
		return this.path != null ? true : false;
	}
}
