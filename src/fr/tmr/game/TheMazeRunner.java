package fr.tmr.game;

import fr.tmr.core.Game;
import fr.tmr.core.gui.Gui;
import fr.tmr.core.gui.IScreen;
import fr.tmr.game.entity.Entity;
import fr.tmr.game.gui.GuiIngame;
import fr.tmr.game.gui.GuiMainMenu;
import fr.tmr.game.level.Level;
import fr.tmr.game.utils.ResourcesLinks;

public class TheMazeRunner extends Game
{
	public static TheMazeRunner instance;

	public static int ZOOM = 5;
	public static final int Block_SIZE = 10;
	public static final int ENTITY_SIZE = (int)(Entity.SIZE * 10);
	public static int Block_SCALE = ZOOM * Block_SIZE;
	public static int ENTITY_SCALE = ZOOM * ENTITY_SIZE;

	public String playerName = "Florian";

	public IScreen currentScreen;
	public Level level;

	public TheMazeRunner()
	{
		TheMazeRunner.instance = this;
	}

	@Override
	public void init()
	{
		ResourcesLinks.checkResources();
		// this.level = Level.loadWorld("level_1");
		// initLevel("level_2");
		Gui.displayGui(new GuiMainMenu());
	}

	public void initLevel(String levelName)
	{
		this.level = new Level().initWorld(levelName);
		// this.currentScreen = level;
	}

	@Override
	public void close()
	{
		if(TheMazeRunner.instance.currentScreen instanceof GuiIngame)
			TheMazeRunner.instance.level.SaveWorld();
		// Engine.threadRender.stop();
		// Engine.threadUpdate.stop();
	}

	// Update l'écran sur lequel on se trouve
	@Override
	public void update(int tick)
	{
		this.currentScreen.update(tick);
	}

	// Rend l'écran sur lequel on se trouve
	@Override
	public void render()
	{
		this.currentScreen.render();
	}

}