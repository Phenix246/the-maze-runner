package fr.tmr.game.items;

public class Item
{
	private int textureId = 1;

	protected boolean isRenderer = true;
	protected String itemName = "";

	public Item(int textureId, String name)
	{
		this.setTextureId(textureId);
		this.setItemName(name);
	}

	public Item setTextureId(int id)
	{
		if(id >= 0 && id < 64)
			this.textureId = id;
		else
			this.textureId = 0;
		return this;
	}

	public int getTextureId(float x, float y)
	{
		return this.textureId;
	}

	public Item setItemName(String name)
	{
		this.itemName = name;
		return this;
	}

	public String getItemName()
	{
		return this.itemName.toLowerCase();
	}

	public String getName()
	{
		return this.getClass().getSimpleName() + this.itemName;
	}
}
