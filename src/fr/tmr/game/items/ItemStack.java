package fr.tmr.game.items;

import fr.tmr.core.graphics.Renderer;
import fr.tmr.game.entity.Entity;
import fr.tmr.game.utils.ResourcesLinks;

public class ItemStack extends Entity
{

	private Item item;
	private int amount;

	public ItemStack(Item item, int amount, float xPos, float yPos)
	{
		super(ITEM_ID);

		this.item = item;
		this.amount = amount;
		this.init(xPos, yPos);
	}

	public ItemStack(Item item, float xPos, float yPos)
	{
		this(item, 1, xPos, yPos);
	}

	public ItemStack(Item item)
	{
		this(item, 1, 0, 0);
	}

	public int getAmount()
	{
		return amount;
	}

	public Item getItem()
	{
		return this.item;
	}

	@Override
	public void render()
	{
		if(this.item.isRenderer)
			Renderer.renderBlock(ResourcesLinks.itemsAtlas, this.getBlockX(), this.getBlockY(), this.item.getTextureId(x, y) % 8, this.item.getTextureId(x, y) / 8);
	}

}
