package fr.tmr.game.utils;

import java.io.File;
import java.nio.ByteBuffer;

import fr.tmr.core.Engine;
import fr.tmr.core.graphics.Texture;
import fr.tmr.core.utils.ByteBufferUtils;

public class ResourcesLinks
{
	// Icons
	public static ByteBuffer icon16 = ByteBufferUtils.createIcon("/assets/icons/icon_16x16.png");
	public static ByteBuffer icon32 = ByteBufferUtils.createIcon("/assets/icons/icon_32x32.png");

	// Fonts
	public static Texture font = new Texture("/assets/textures/font.png");

	// Textures
	public static Texture blocksAtlas = new Texture("/assets/textures/blocks.png");
	public static Texture itemsAtlas = new Texture("/assets/textures/items.png");
	public static Texture playerAtlas = new Texture("/assets/textures/player.png");

	// Buttons
	public static Texture playButton = new Texture("/assets/textures/button.png");

	// Folders
	public static File assets = new File(Engine.path + "/assets/");
	public static File saves = new File(Engine.path + "/saves/");

	public static void checkResources()
	{
		// checkDir(assets);
		checkDir(saves);
	}

	private static void checkDir(File dir)
	{
		if(!dir.exists())
			dir.mkdirs();
	}
}
