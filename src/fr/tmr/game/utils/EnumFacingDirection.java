package fr.tmr.game.utils;

public enum EnumFacingDirection
{
	FORWARD, LEFT, BACKWARD, RIGHT;
}