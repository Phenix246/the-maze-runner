package fr.tmr.game.utils;

public enum GameZoom
{
	SMALL(2),
	NORMAL(5),
	LARGE(8);

	private int zoom;

	GameZoom(int zoom)
	{
		this.zoom = zoom;
	}

	public int getZoom()
	{
		return this.zoom;
	}
}
