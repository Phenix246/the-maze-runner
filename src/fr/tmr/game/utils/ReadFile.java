package fr.tmr.game.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ReadFile
{
	public static BufferedReader read(String path)
	{

		File f = new File(path);
		System.out.println("absolue path : " + f.getAbsolutePath());
		// lecture du fichier texte
		try
		{
			InputStream ips = new FileInputStream(f);
			InputStreamReader ipsr = new InputStreamReader(ips, "UTF-8");
			BufferedReader br = new BufferedReader(ipsr);
			return br;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
