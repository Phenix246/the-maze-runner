package fr.tmr.game.block;

public class BlockList
{
	public static final Block AIR = new BlockAir();
	public static final Block WALL = new BlockWall();
	public static final Block END = new BlockEnd();
	public static final Block START = new BlockStart();

}
