package fr.tmr.game.block;

import fr.tmr.core.graphics.Renderer;
import fr.tmr.game.utils.ResourcesLinks;

public class Block // extends GameObject
{

	private boolean isSolid = true;
	protected boolean isRenderer = true;

	private int textureId = 1;

	public Block(int textureId)
	{
		this.setTextureId(textureId);
	}

	public void setTextureId(int id)
	{
		if(id >= 0 && id < 64)
			this.textureId = id;
		else
			this.textureId = 0;
	}

	public Block setSolid(boolean b)
	{
		this.isSolid = b;
		return this;
	}

	public boolean isSolid()
	{
		return this.isSolid;
	}

	public int getTextureId(int x, int y)
	{
		return this.textureId;
	}

	public void render(int x, int y)
	{
		if(this.isRenderer)
			Renderer.renderBlock(ResourcesLinks.blocksAtlas, x, y, this.getTextureId(x, y) % 8, this.getTextureId(x, y) / 8);
	}

	public String getName()
	{
		return this.getClass().getSimpleName();
	}

}
