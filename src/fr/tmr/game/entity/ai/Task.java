package fr.tmr.game.entity.ai;

import fr.tmr.core.Engine;

public abstract class Task
{
	public enum State
	{
		START,
		RUN,
		WAIT,
		END;
	}

	public State state = State.START;

	protected AIBase ai;

	public Task(AIBase ai)
	{
		this.ai = ai;
	}

	public boolean execute()
	{
		if(state.equals(State.START))
			return start();
		if(state.equals(State.RUN))
			return update(Engine.ticks);
		if(state.equals(State.WAIT))
			return sleep();
		if(state.equals(State.END))
			return finish();
		return false;
	}

	public abstract boolean start();

	public abstract boolean update(int ticks);

	public abstract boolean sleep();

	public abstract boolean finish();
}
