package fr.tmr.game.entity.ai;

import fr.tmr.game.entity.Entity;

public class AIBase
{
	private Task currentTask = new TaskWait(this);
	private Task previousTask;
	public Entity target = null, entity = null;

	public AIBase(Entity entity)
	{
		this.entity = entity;
	}

	public void setTarget(Entity go)
	{
		target = go;
	}

	public Entity getTarget()
	{
		return target;
	}

	public void changeTask(Task newTask)
	{
		if(currentTask != newTask)
		{
			if(currentTask != null)
			{
				previousTask = currentTask;
				previousTask.state = Task.State.END;
				previousTask.execute();
			}
			currentTask = newTask;
			currentTask.state = Task.State.START;
			currentTask.execute();
			currentTask.state = Task.State.RUN;
		}
	}

	private TaskHunt hunt;

	public Task getNextAction()
	{
		return new TaskLook(this);
		/**
		 * if(hunt == null) hunt = new TaskHunt(this); if(target == null) { if(currentTask
		 * instanceof TaskHunt) { hunt = (TaskHunt)currentTask; if(!hunt.pathfinder.hasPath())
		 * changeTask(new TaskLook(this));
		 * 
		 * } else changeTask(new TaskLook(this)); } else {
		 * if(MathsHelper.distance2d(entity.getBlockX(), entity.getBlockY(),
		 * getTarget().getBlockX(), getTarget().getBlockY()) <= entity.attackRange) changeTask(new
		 * TaskAttack(this)); else changeTask(hunt); } System.out.println("task : " +
		 * currentTask.getClass()); return currentTask;
		 **/
	}
}
