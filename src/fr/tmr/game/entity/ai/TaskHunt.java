package fr.tmr.game.entity.ai;

import fr.tmr.core.maths.MathsHelper;
import fr.tmr.game.entity.Entity;
import fr.tmr.game.pathfinder.Node;
import fr.tmr.game.pathfinder.PathfinderManhattan;

public class TaskHunt extends Task
{
	public PathfinderManhattan pathfinder;

	Entity target;
	Entity hunter;

	// public PathFinder pathFinder;

	public TaskHunt(AIBase ai)
	{
		super(ai);
	}

	@Override
	public boolean start()
	{
		this.target = super.ai.getTarget();
		this.hunter = super.ai.entity;
		this.pathfinder = new PathfinderManhattan(this.target.level);
		// System.out.println("Start hunt");
		return true;
	}

	@Override
	public boolean update(int ticks)
	{
		Node start = new Node((int)this.hunter.getBlockX(), (int)this.hunter.getBlockY());
		Node end = new Node((int)this.target.getBlockX(), (int)this.target.getBlockY());
		if(ticks % 3 == 0)
			this.pathfinder.find(start, end);
		this.hunter.goTo(pathfinder);

		// test if the target is out of range
		if(MathsHelper.distance2d(hunter.getBlockX(), hunter.getBlockY(), target.getBlockX(), target.getBlockY()) > hunter.sightRange * 1.5F)
			super.ai.target = null;

		// movez

		// Point point = this.pathFinder.getNextPoint();
		hunter.move(target.getX() - hunter.getBlockX(), target.getY() - hunter.getBlockY());
		// hunter.move(target.getX() - hunter.getX(), target.getY() -
		// hunter.getY());
		return false;
	}

	@Override
	public boolean finish()
	{
		return false;
	}

	@Override
	public boolean sleep()
	{
		return false;
	}

}
