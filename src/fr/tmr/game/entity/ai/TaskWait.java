package fr.tmr.game.entity.ai;

public class TaskWait extends Task
{

	public TaskWait(AIBase ai)
	{
		super(ai);
	}

	@Override
	public boolean start()
	{
		return false;
	}

	@Override
	public boolean update(int ticks)
	{
		return false;
	}

	@Override
	public boolean finish()
	{
		return false;
	}

	@Override
	public boolean sleep()
	{
		return false;
	}

}
