package fr.tmr.game.entity.ai;

import java.util.ArrayList;

import fr.tmr.core.physics.Physics;
import fr.tmr.core.utils.GameObject;
import fr.tmr.game.entity.Entity;

public class TaskLook extends Task
{

	public TaskLook(AIBase ai)
	{
		super(ai);
	}

	@Override
	public boolean start()
	{
		ArrayList<Entity> objects = Physics.sphereCollide(super.ai.entity.getBlockX(), super.ai.entity.getBlockY(), super.ai.entity.sightRange);

		for(Entity go : objects)
			if(go.getType() == GameObject.PLAYER_ID)
				super.ai.setTarget(go);
		return true;
	}

	@Override
	public boolean update(int ticks)
	{
		return false;
	}

	@Override
	public boolean finish()
	{
		return false;
	}

	@Override
	public boolean sleep()
	{
		return false;
	}

}
