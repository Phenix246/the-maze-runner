package fr.tmr.game.entity.ai;

public class TaskAttack extends Task
{

	public TaskAttack(AIBase ai)
	{
		super(ai);
	}

	@Override
	public boolean start()
	{
		return false;
	}

	@Override
	public boolean update(int ticks)
	{
		if(super.ai.entity.attackDelay.isOver())
		{
			super.ai.getTarget().damage(super.ai.entity.getAttackDamage());
			System.out.println("We're hit! :" + super.ai.getTarget().getCurrentHealth() + "/" + super.ai.getTarget().getMaxHealth());
			super.ai.entity.attackDelay.restart();
			return true;
		}
		return false;
	}

	@Override
	public boolean finish()
	{
		return false;
	}

	@Override
	public boolean sleep()
	{
		return false;
	}

}
