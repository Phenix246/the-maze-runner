package fr.tmr.game.entity;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import fr.tmr.core.physics.Physics;
import fr.tmr.core.time.Delay;
import fr.tmr.core.utils.GameObject;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.entity.ai.AIBase;
import fr.tmr.game.level.Level;
import fr.tmr.game.pathfinder.Node;
import fr.tmr.game.pathfinder.PathfinderManhattan;
import fr.tmr.game.utils.EnumFacingDirection;

public class Entity extends GameObject
{
	public static final float SIZE = 0.7F;
	protected static final int TOP = 0, RIGHT = 1, BOTTOM = 2, LEFT = 3;

	public float sightRange = 1;
	public int attackRange = 1;
	protected int attackDamage = 0;
	protected float catchRange = 0.5F;
	protected float fogRange = 1.5F;

	protected EnumFacingDirection facingDirection;
	public Delay attackDelay;

	// Motion
	protected int moveAmountX = 0;
	protected int moveAmountY = 0;
	protected float drag = 0.95F;

	// Stats
	public int health = 100;
	protected int maxHealth = 100;
	protected float speed = 1F;

	// Others
	protected int id;
	protected AIBase ai;

	// Utils
	public Level level;

	public Entity(int id)
	{
		super(id);
		this.id = id;
		// this.level = level;
		this.ai = new AIBase(this);
		attackDelay = new Delay(500);
		attackRange = 49;
		attackDamage = 1;
		facingDirection = EnumFacingDirection.FORWARD;
		attackDelay.terminate();
	}

	// Stats
	public void setAttackDamage(int amt)
	{
		this.attackDamage = amt;
	}

	public int getAttackDamage()
	{
		return this.attackDamage;
	}

	public int damage(int account)
	{
		return this.health -= account;
	}

	public int getCurrentHealth()
	{
		return this.health;
	}

	public int getMaxHealth()
	{
		return this.maxHealth;
	}

	// Render
	@Override
	public void render()
	{
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(x, y, 0);
		}
		GL11.glPopMatrix();
	}

	@Override
	public void update(int ticks)
	{
		// test if the entity is dead
		if((health <= 0))
			death();

		// Inertia system

		float magX = this.moveAmountX / this.speed;
		float magY = this.moveAmountY / this.speed;

		// test limits
		if((this.getBlockX() <= 0 && magX < 0) || (this.getBlockY() <= 0 && magY < 0) || ((this.getBlockX() + 1) > TheMazeRunner.instance.level.worldLimitX && magX > 0) || ((this.getBlockY() + 1) > TheMazeRunner.instance.level.worldLimitY && magY > 0))
			return;

		// test solid block
		int moveX = Math.abs(this.moveAmountX * TheMazeRunner.Block_SCALE);
		int moveY = Math.abs(this.moveAmountY * TheMazeRunner.Block_SCALE);

		for(int i = 0; i < moveX; i++)
			if(!Physics.isBlockCollideX(this, magX / TheMazeRunner.Block_SCALE))
				this.x += magX / TheMazeRunner.Block_SCALE;

		for(int i = 0; i < moveY; i++)
			if(!Physics.isBlockCollideY(this, -magY / TheMazeRunner.Block_SCALE))
				this.y -= magY / TheMazeRunner.Block_SCALE;

		this.moveAmountX *= drag;
		this.moveAmountY *= drag;
	}

	public void move(float magX, float magY)
	{
		// format informations
		if(magX < 0)
			magX = -1;
		if(magX > 0)
			magX = 1;

		if(magY < 0)
			magY = -1;
		if(magY > 0)
			magY = 1;

		// Choose the direction
		if(magX == 0 && magY == 1)
			this.facingDirection = EnumFacingDirection.FORWARD;
		if(magX == -1 && magY == 0)
			this.facingDirection = EnumFacingDirection.LEFT;
		if(magX == 0 && magY == -1)
			this.facingDirection = EnumFacingDirection.BACKWARD;
		if(magX == 1 && magY == 0)
			this.facingDirection = EnumFacingDirection.RIGHT;

		// test limits
		if((this.getBlockX() <= 0 && magX < 0) || (this.getBlockY() <= 0 && magY < 0) || ((this.getBlockX() + 1) >= TheMazeRunner.instance.level.worldLimitX && magX > 0) || ((this.getBlockY() + 1) >= TheMazeRunner.instance.level.worldLimitY && magY > 0))
			return;

		// Set helpful variables
		this.moveAmountX = (int)(this.speed * magX);
		this.moveAmountY = (int)(this.speed * magY);

		// test solid block
		int moveX = Math.abs(this.moveAmountX * TheMazeRunner.Block_SCALE);
		int moveY = Math.abs(this.moveAmountY * TheMazeRunner.Block_SCALE);

		for(int i = 0; i < moveX; i++)
			if(!Physics.isBlockCollideX(this, magX / TheMazeRunner.Block_SCALE))
				this.x += magX / TheMazeRunner.Block_SCALE;

		for(int i = 0; i < moveY; i++)
			if(!Physics.isBlockCollideY(this, -magY / TheMazeRunner.Block_SCALE))
				this.y -= magY / TheMazeRunner.Block_SCALE;

	}

	public void goTo(float x, float y)
	{
		this.move(x - this.getBlockX(), this.getBlockY() - y);
	}

	public void goTo(PathfinderManhattan pathfinder)
	{
		ArrayList<Node> path = pathfinder.getPath();
		if(path != null)
		{
			if(path.size() >= 3)
				this.goTo(path.get(2).x + 0.1f, path.get(2).y + 0.1f);
			else if(path.size() >= 2)
				this.goTo(path.get(1).x + 0.1f, path.get(1).y + 0.1f);
		}
	}

	public boolean attack()
	{
		return false;
		// System.out.println("We're attacking!");
		//
		// ArrayList<Entity> objects = new ArrayList<Entity>();
		//
		// if(facingDirection == EnumFacingDirection.FORWARD)
		// objects = TheMazeRunner.rectangleCollide(x, y, x + SIZE, y +
		// attackRange + SIZE);
		// else if(facingDirection == EnumFacingDirection.LEFT)
		// objects = TheMazeRunner.rectangleCollide(x, y, x - attackRange +
		// SIZE, y + SIZE);
		// else if(facingDirection == EnumFacingDirection.BACKWARD)
		// objects = TheMazeRunner.rectangleCollide(x, y - attackRange + SIZE, x
		// + SIZE, y);
		// else if(facingDirection == EnumFacingDirection.RIGHT)
		// objects = TheMazeRunner.rectangleCollide(x, y, x + attackRange, y +
		// SIZE);
		//
		// ArrayList<Entity> enemies = new ArrayList<Entity>();
		//
		// for(GameObject go : objects)
		// {
		// if(go instanceof Entity)
		// enemies.add((Entity)go);
		// }
		//
		// if(enemies.size() > 0)
		// {
		// Entity target = enemies.get(0);
		// if(enemies.size() > 1)
		// {
		// for(Entity e : enemies)
		// if(Util.dist(x, y, e.getBlockX(), e.getBlockY()) < Util.dist(x, y,
		// target.getBlockX(), target.getBlockY()))
		// target = e;
		// }
		//
		// target.damage(attackDamage);
		// System.out.println(" : " + target.health + "/" + target.maxHealth);
		// }
		// else
		// System.out.println(" : No target");
		//
		// attackDelay.restart();
		// return true;
	}

	public boolean death()
	{
		remove();
		return true;
	}

	public int getHealth()
	{
		return this.health;
	}

	// Coordinated methods

	public float getBlockX()
	{
		return this.getX() / TheMazeRunner.Block_SCALE;
	}

	public float getBlockY()
	{
		return this.getY() / TheMazeRunner.Block_SCALE;
	}

	public float getBlockX(float value)
	{
		return (this.getX() + value) / TheMazeRunner.Block_SCALE;
	}

	public float getBlockY(float value)
	{
		return (this.getY() + value) / TheMazeRunner.Block_SCALE;
	}

	// Init
	public GameObject init(Level level, float x, float y)
	{
		this.level = level;
		this.x = x * TheMazeRunner.Block_SCALE;
		this.y = y * TheMazeRunner.Block_SCALE;
		return this;
	}
}