package fr.tmr.game.entity;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import fr.tmr.core.graphics.Animation;
import fr.tmr.core.maths.MathsHelper;
import fr.tmr.core.physics.Physics;
import fr.tmr.core.time.Delay;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.achievement.AchievementStats;
import fr.tmr.game.items.ItemList;
import fr.tmr.game.items.ItemStack;
import fr.tmr.game.pathfinder.Node;
import fr.tmr.game.pathfinder.PathfinderManhattan;
import fr.tmr.game.utils.ResourcesLinks;

public class EntityPlayer extends Entity
{
	public int score = 0;
	public int fogCatch = 0;
	private AchievementStats aStats;
	public int livesLeft = 3;
	public boolean hasWin = false;
	public boolean cheatPath = false;
	Animation animation;
	private Delay cheatDelay = new Delay(1000);

	// PathFinder : distance de Manhattan
	public PathfinderManhattan pathfinder;

	public EntityPlayer()
	{
		super(PLAYER_ID);
		this.attackDelay = new Delay(500);
		this.attackRange = 49;
		this.attackDamage = 1;
		this.speed = 4F;
		this.catchRange = 0.75F;
		this.fogRange = 1.75F;
		this.aStats = new AchievementStats();
		this.animation = new Animation(ResourcesLinks.playerAtlas, 200).start();
		this.cheatDelay.start();

	}

	@Override
	public void update(int ticks)
	{
		super.update(ticks);
		this.look();
		if(ticks % 3 == 0)
			this.getPathtoEnd();

		// test pour la fin de jeu
		if(MathsHelper.distance2d(this.getBlockX(), this.getBlockY(), this.level.xEndCoor, this.level.yEndCoor) < 0.5F)
			this.hasWin = true;
		else
			this.hasWin = false;

		// triche : va à la fin du niveau
		if(this.cheatPath)
			this.goTo(pathfinder);
	}

	public void getPathtoEnd()
	{
		this.pathfinder = new PathfinderManhattan(this.level);
		this.pathfinder.find(new Node((int)this.getBlockX(), (int)this.getBlockY()), new Node(this.level.xEndCoor, this.level.yEndCoor));
	}

	public void getInput()
	{
		if(Keyboard.isKeyDown(Keyboard.KEY_Z) || Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP))
		{
			this.move(0, 1);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_Q) || Keyboard.isKeyDown(Keyboard.KEY_LEFT))
		{
			this.move(-1, 0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_S) || Keyboard.isKeyDown(Keyboard.KEY_DOWN))
		{
			this.move(0, -1);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_D) || Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
		{
			this.move(1, 0);
		}
		if((Keyboard.isKeyDown(Keyboard.KEY_SPACE) || Keyboard.isKeyDown(Keyboard.KEY_0)) && attackDelay.isOver())
			this.attack();
		if(Keyboard.isKeyDown(Keyboard.KEY_C))
		{
			if(this.cheatPath && cheatDelay.isOver())
				this.cheatPath = false;
			else
				this.cheatPath = true;
		}
	}

	@Override
	public void render()
	{
		if(this.moveAmountX != 0 || this.moveAmountY != 0)
			this.animation.start();
		else
			this.animation.stop();
		this.animation.render(x, y);
	}

	@Override
	public boolean death()
	{
		this.livesLeft--;
		System.out.println("lives left : " + this.livesLeft);

		if(this.livesLeft == 0)
		{
			remove();
			return true;
		}
		else
		{
			this.health = this.maxHealth;
			this.init(this.level, this.level.xSpawnCoor, this.level.ySpawnCoor);
		}
		return false;
	}

	public boolean look()
	{
		// Treasure and keys
		ArrayList<Entity> objects = Physics.sphereCollide(this.getX() + TheMazeRunner.ENTITY_SIZE / 2, this.getY() + TheMazeRunner.ENTITY_SIZE / 2, this.catchRange);

		for(Entity go : objects)
		{
			if(go.getType() == ITEM_ID)
			{
				ItemStack stack = (ItemStack)go;
				if(stack.getItem() != ItemList.FOG)
				{
					this.score += stack.getAmount();
					stack.remove();
					System.out.println("You score is: " + this.score);
				}
			}
		}

		// Fog
		ArrayList<Entity> fog = Physics.sphereCollide(this.getX() + TheMazeRunner.ENTITY_SIZE / 2, this.getY() + TheMazeRunner.ENTITY_SIZE / 2, this.fogRange);

		for(Entity entity : fog)
		{
			if(entity.getType() == ITEM_ID)
			{
				ItemStack stack = (ItemStack)entity;
				if(stack.getItem() == ItemList.FOG)
				{
					stack.remove();
					this.fogCatch++;
				}
			}
		}

		// Test achievements
		// Explorer achievement
		if(this.level.maxFog == this.fogCatch)
			if(this.aStats.explorer.triggerAchievement())
				System.out.println("achievement get : explorer");

		// Rich achievement
		if(this.level.maxTreasure == this.score)
			if(this.aStats.rich.triggerAchievement())
				System.out.println("achievement get : rich");

		return true;
	}

}