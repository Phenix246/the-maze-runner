package fr.tmr.game.entity;

import fr.tmr.core.time.Delay;

public class EntityEnemy extends Entity
{
	// public static final int FORWARD = 0;
	// public static final int LEFT = 1;
	// public static final int BACKWARD = 2;
	// public static final int RIGHT = 3;
	// public static final float DAMPING = 0.5F;

	public Delay attackDelay;

	public EntityEnemy(int level)
	{
		super(ENEMY_ID);

		// stats = new Stats(level, false);
		attackDelay = new Delay(500);
		attackRange = 1;
		attackDamage = 1;
		moveAmountX = 0;
		moveAmountY = 0;
		attackDelay.terminate();
		sightRange = 6; // 6
		this.speed = 1F;
	}

	@Override
	public void update(int ticks)
	{
		super.update(ticks);
		if(this.ai != null)
			ai.getNextAction().execute();
	}

	public void setAttackRange(int attackRange)
	{
		this.attackRange = attackRange;
	}

	public void setAttackDelay(int time)
	{
		this.attackDelay = new Delay(time);
		attackDelay.terminate();
	}

	public void setSightRange(float dist)
	{
		this.sightRange = dist;
	}

}