package fr.tmr.game.gui;

import java.util.Random;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import fr.tmr.core.Engine;
import fr.tmr.core.color.Color;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.level.Level;

public class GuiIngame extends GuiScreen
{
	String levelName;
	Level level;

	private String[] levels = new String[] {"level_0", "level_1", "level_2", "level_3"};

	public GuiIngame(String levelName)
	{
		TheMazeRunner.instance.level = new Level().initWorld(levelName);
		this.level = TheMazeRunner.instance.level;
	}

	@Override
	public void init()
	{
		this.setBackgroundGui(this.level);
	}

	@Override
	public void update(int tick)
	{
		super.update(tick);
	}

	@Override
	public void drawScreen(int width, int height)
	{

		// Si l'arrière plan n'est pas nul on le rend
		if(this.background != null)
		{
			GL11.glTranslatef(0, 32, 0);
			this.background.render();
			GL11.glTranslatef(0, -32, 0);
		}

		// On rend les boutons
		for(GuiButton button : this.buttonList)
		{
			button.drawButton();
		}

		this.drawRect(0, 0, width, 32, Color.SILVER);
		if(Engine.debug)
		{
			Display.setTitle("The Maze Runner - FPS: " + Engine.frames + " - TPS: " + Engine.ticks);
		}
		this.drawString(width - sizeofString("Score :" + this.level.player.score, 24), 3, "Score :" + this.level.player.score, 24, Color.GREEN);
		if(this.level.player.hasWin)
		{
			this.drawString(40, 64, "You win", 40, Color.GOLD);
			this.drawString(20, 110, "Your score is:" + this.level.player.score, 40, Color.GOLD);
			this.level.initWorld(this.levels[new Random().nextInt(this.levels.length)]);
		}
	}

	@Override
	public void actionPerformed(int buttonId)
	{}
}
