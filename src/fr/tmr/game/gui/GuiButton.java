package fr.tmr.game.gui;

import fr.tmr.core.graphics.Renderer;
import fr.tmr.core.graphics.Texture;
import fr.tmr.core.gui.Gui;

public class GuiButton extends Gui
{
	private int id;
	private int x, y, width, height, size;
	private Texture texture;
	private String text;

	private static float[] color = new float[] {0.5F, 0, 0, 1};

	public GuiButton(int id, int x, int y, int width, int height, Texture texture)
	{
		// Appel de l'autre constructeur de la classe (GuiButton)
		this(id, x, y, width, height, texture, "");
	}

	public GuiButton(int id, int x, int y, int width, int height, Texture texture, String tex)
	{
		this(id, x, y, width, height, texture, "", 20);
	}

	public GuiButton(int id, int x, int y, int width, int height, Texture texture, String text, int size)
	{
		this.id = id;
		this.x = x;
		this.y = y;
		this.texture = texture;
		this.width = width;
		this.height = height;
		this.text = text;
		this.size = size;
	}

	public void drawButton()
	{
		// Renderer.renderColor(this.x, this.y, this.width, this.height, color);
		// Main.font.drawString(this.x, this.y, this.text, Color.blue);
		Renderer.renderTexture(this.texture, this.x, this.y, this.width, this.height, 0, 0);
		this.drawCenteredString(this.x + (this.width / 2), this.y + (this.height / 2) - 10, this.text, this.size);
	}

	public void updateButton(int tick)
	{}

	// Retourne l'id du boutton
	public int getId()
	{
		return this.id;
	}

	public boolean isCoors(int xPos, int yPos)
	{
		// On vérifie si les coordonnées de la souris sont contenues dans les coordonnées du bouton
		// et que l'on clique sur le boutton gauche de la souris
		if((xPos >= this.x && xPos < (this.x + this.width)) && (yPos >= this.y && yPos < (this.y + this.height)))
			return true;
		return false;
	}
}
