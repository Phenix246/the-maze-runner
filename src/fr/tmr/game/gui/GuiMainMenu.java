package fr.tmr.game.gui;

import fr.tmr.core.Engine;
import fr.tmr.core.gui.Gui;
import fr.tmr.game.utils.ResourcesLinks;

public class GuiMainMenu extends GuiScreen
{
	public GuiMainMenu()
	{}

	@Override
	public void init()
	{
		buttonList.add(new GuiButton(0, 10, 10, 400, 100, ResourcesLinks.playButton, "JOUER", 40));
		buttonList.add(new GuiButton(1, 10, 140, 400, 100, ResourcesLinks.playButton, "OPTIONS", 40));
		buttonList.add(new GuiButton(2, 10, 280, 400, 100, ResourcesLinks.playButton, "QUITTER", 40));
	}

	@Override
	public void drawScreen(int width, int height)
	{
		super.drawScreen(width, height);
	}

	@Override
	public void update(int tick)
	{
		super.update(tick);
	}

	@Override
	public void actionPerformed(int buttonId)
	{
		switch(buttonId)
		{
		case 0:
			System.out.println("Jouer");
			Gui.displayGui(new GuiIngame("level_3"));
			break;
		case 1:
			break;
		case 2:
			Engine.isRunning = false;
			break;
		}
	}

}
