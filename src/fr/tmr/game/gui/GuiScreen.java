package fr.tmr.game.gui;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;

import fr.tmr.core.Engine;
import fr.tmr.core.gui.Gui;
import fr.tmr.core.gui.IScreen;

public class GuiScreen extends Gui implements IScreen
{
	public ArrayList<GuiButton> buttonList = new ArrayList<GuiButton>();
	protected IScreen background;

	public GuiScreen()
	{}

	public void init()
	{}

	public void setBackgroundGui(IScreen gui)
	{
		if(gui != null)
			this.background = gui;
	}

	public void drawScreen(int width, int height)
	{
		// Si l'arrière plan n'est pas nul on le rend
		if(this.background != null)
			this.background.render();
		// On rend les boutons
		for(GuiButton button : this.buttonList)
		{
			button.drawButton();
		}
	}

	@Override
	public void update(int tick)
	{
		// Si l'arrière plan n'est pas nul on l'update
		if(this.background != null)
			this.background.update(tick);
		// On update le bouton
		for(GuiButton button : buttonList)
		{
			button.updateButton(tick);
		}

		// On obtient les coordonnées X et Y du curseur

		while(Mouse.next())
		{
			if(Mouse.getEventButtonState())
			{
				if(Mouse.getEventButton() == 0)
				{
					int xPos = Mouse.getEventX();
					int yPos = Engine.height - Mouse.getEventY();

					for(GuiButton button : this.buttonList)
					{
						if(button.isCoors(xPos, yPos))
						{
							int id = button.getId();
							System.out.println("id: " + id + " x: " + xPos + " y: " + yPos);
							this.actionPerformed(id);
						}
					}
				}
			}

		}

		if(Mouse.isButtonDown(0))
		{}
	}

	public void actionPerformed(int buttonId)
	{}

	@Override
	public void render()
	{
		this.drawScreen(Engine.width, Engine.height);
	}
}
