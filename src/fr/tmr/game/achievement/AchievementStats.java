package fr.tmr.game.achievement;

public class AchievementStats
{
	public Achievement explorer;
	public Achievement rich;

	public AchievementStats()
	{
		this.explorer = new Achievement("explorer");
		this.rich = new Achievement("rich");
	}
}
