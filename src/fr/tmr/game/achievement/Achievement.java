package fr.tmr.game.achievement;

public class Achievement
{
	private boolean isSuccessful = false;

	private String name;

	public Achievement(String name)
	{
		this.name = name;
	}

	public boolean triggerAchievement()
	{
		if(this.isSuccessful == false)
		{
			this.isSuccessful = true;
			return true;
		}
		return false;
	}

	public boolean isSuccessful()
	{
		return this.isSuccessful;
	}

	public String getName()
	{
		return this.name;
	}
}
