package fr.tmr.game;

import java.nio.ByteBuffer;

import fr.tmr.core.Engine;
import fr.tmr.game.utils.ResourcesLinks;

public class Main
{

	public static void main(String[] args)
	{
		// on démarre le moteur de jeu
		Engine.initEngine(1280, 720, true, false, "The Maze Runner", new TheMazeRunner());

		// Convertit les images en ButeBuffer afin de les définir comme icones
		ByteBuffer[] icons = new ByteBuffer[2];

		icons[0] = ResourcesLinks.icon16;
		icons[1] = ResourcesLinks.icon32;

		// On définie les icones de la fenêtre
		Engine.setIcons(icons);

		// on lance la boucle principale du jeu
		Engine.engineLoop();
	}

}
