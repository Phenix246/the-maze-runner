package fr.tmr.game.level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.block.Block;
import fr.tmr.game.block.BlockList;
import fr.tmr.game.entity.EntityPlayer;
import fr.tmr.game.items.Item;
import fr.tmr.game.items.ItemList;
import fr.tmr.game.items.ItemStack;
import fr.tmr.game.utils.ResourcesLinks;

public class MapReader
{
	public static Level getMap(String name)
	{
		Level level = new Level();

		ArrayList<String> map = readMap(name);

		int mode = 0;

		int playerX = 1, playerY = 1;

		for(int i = 0; i < map.size(); i++)
		{
			String text = map.get(i);

			// Get mode
			if(text.toCharArray()[0] == '#')
			{
				mode = getMode(text);
				continue;
			}
			String[] infos = text.split(MapSaver.separator);
			//
			switch(mode)
			{
			// World
			case 0:
				level.worldLimitX = Integer.parseInt(infos[0]);
				level.worldLimitY = Integer.parseInt(infos[1]);
				level.worldName = infos[2];
				level.xSpawnCoor = Integer.parseInt(infos[3]);
				level.ySpawnCoor = Integer.parseInt(infos[4]);
				level.xEndCoor = Integer.parseInt(infos[5]);
				level.yEndCoor = Integer.parseInt(infos[6]);
				level.maxTreasure = Integer.parseInt(infos[7]);
				level.maxFog = Integer.parseInt(infos[8]);
				level.blockMap = new Block[level.worldLimitX][level.worldLimitY];
				break;
			case 1:
				System.out.println(text);
				// Init entities
				level.player = new EntityPlayer();
				level.player.health = Integer.parseInt(infos[2]);
				level.player.livesLeft = Integer.parseInt(infos[3]);
				level.player.score = Integer.parseInt(infos[4]);
				level.player.fogCatch = Integer.parseInt(infos[5]);
				playerX = Integer.parseInt(infos[0]);
				playerY = Integer.parseInt(infos[1]);
				level.player.init(level, playerX, playerY);
				level.player.x = playerX * TheMazeRunner.Block_SCALE;
				level.player.y = playerY * TheMazeRunner.Block_SCALE;
				// Register entities
				level.entitiesList.add(level.player);

				break;
			// Block
			case 2:
				level.setBlock(Integer.parseInt(infos[0]), Integer.parseInt(infos[1]), getBlock(infos[2]));
				break;
			case 3:
				level.entitiesList.add(new ItemStack(getItemStack(infos[2]), Integer.parseInt(infos[3]), Float.parseFloat(infos[0]), Float.parseFloat(infos[1])));
				break;

			}

		}
		level.entitiesList.get(0).init(level, playerX, playerY);

		return level;
	}

	private static Item getItemStack(String text)
	{
		switch(text)
		{
		default:
		case "ItemFog":
			return ItemList.FOG;
		case "ItemDiamond":
			return ItemList.DIAMOND;
		}
	}

	private static Block getBlock(String text)
	{
		switch(text)
		{
		default:
		case "BlockWall":
			return BlockList.WALL;
		case "BlockAir":
			return BlockList.AIR;
		}
	}

	private static int getMode(String line)
	{
		System.out.println("mode : " + line);
		switch(line)
		{
		case "#WORLD#":
			return 0;
		case "#PLAYER#":
			return 1;
		case "#BLOCKS#":
			return 2;
		case "#ITEMS#":
			return 3;
		default:
			return 0;
		}
	}

	private static ArrayList<String> readMap(String name)
	{
		File directory = new File(ResourcesLinks.saves + "/" + TheMazeRunner.instance.playerName + "/");
		File file = new File(directory + "/" + name + ".map");

		FileInputStream fis;
		System.out.println("read into : " + file.getAbsolutePath());
		ArrayList<String> map = new ArrayList<String>();

		try
		{
			fis = new FileInputStream(file);
			InputStreamReader ipsr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(ipsr);

			String ligne;

			while((ligne = br.readLine()) != null)
			{
				map.add(ligne);
			}
			br.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return map;
	}
}
