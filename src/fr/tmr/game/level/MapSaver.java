package fr.tmr.game.level;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import fr.tmr.core.utils.GameObject;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.entity.Entity;
import fr.tmr.game.entity.EntityPlayer;
import fr.tmr.game.items.ItemStack;
import fr.tmr.game.utils.ResourcesLinks;

public class MapSaver
{

	public static String separator = "::";

	public static boolean saveMap(Level level)
	{
		File directory = new File(ResourcesLinks.saves + "/" + TheMazeRunner.instance.playerName + "/");
		File file = new File(directory + "/" + level.worldName + ".map");

		FileWriter save;
		String[] blocks = getAllBlocks(level);
		String[] items = getAllItems(level);
		// System.out.println("write into : " + file.getAbsolutePath());

		try
		{
			if(!directory.exists())
				directory.mkdirs();
			if(!file.exists())
				file.createNewFile();

			save = new FileWriter(file);

			// World Infos
			save.write("#WORLD#" + "\n");
			save.write(level.worldLimitX + separator);
			save.write(level.worldLimitY + separator);
			save.write(level.worldName + separator);
			save.write(level.xSpawnCoor + separator);
			save.write(level.ySpawnCoor + separator);
			save.write(level.xEndCoor + separator);
			save.write(level.yEndCoor + separator);
			save.write(level.maxTreasure + separator);
			save.write(level.maxFog + separator);
			save.write("\n");

			// Player
			save.write("#PLAYER#" + "\n");
			EntityPlayer player = (EntityPlayer)level.entitiesList.get(0);
			save.write((int)(player.x / TheMazeRunner.Block_SCALE) + separator);
			save.write((int)(player.y / TheMazeRunner.Block_SCALE) + separator);
			save.write(player.health + separator);
			save.write(player.livesLeft + separator);
			save.write(player.score + separator);
			save.write(player.fogCatch + separator);
			save.write("\n");

			// Blocks
			save.write("#BLOCKS#" + "\n");
			for(int i = 0; i < blocks.length; i++)
				save.write(blocks[i] + "\n");

			// Items
			save.write("#ITEMS#" + "\n");
			for(int i = 0; i < items.length; i++)
				save.write(items[i] + "\n");

			// save.write("#END#");
			save.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return true;
	}

	private static String[] getAllBlocks(Level level)
	{
		// Initialisation de quelques variables utiles
		int maxX = level.worldLimitX + 1;
		int maxY = level.worldLimitY + 1;
		int index = 0;
		String[] list = new String[maxX * maxY];

		for(int x = 0; x < maxX; x++)
		{
			for(int y = 0; y < maxY; y++)
			{
				list[index] = getBlock(level, x, y);
				index++;
			}
		}
		// On retourne la liste
		return list;
	}

	private static String[] getAllItems(Level level)
	{
		// Initialisation de quelques variables utiles
		ArrayList<String> listInter = new ArrayList<String>();
		// Ajoute tout les items dans une première list
		for(Entity entity : level.entitiesList)
		{
			if(entity.getType() == GameObject.ITEM_ID)
				listInter.add(getItem((ItemStack)entity));
		}
		// Transfère tout les items dans une seconde list avec juste la bonne taille
		String[] list = new String[listInter.size()];
		for(int i = 0; i < listInter.size(); i++)
			list[i] = listInter.get(i);
		// On retourne la liste
		return list;
	}

	private static String getBlock(Level level, int x, int y)
	{
		return x + separator + y + separator + level.getBlock(x, y).getName();
	}

	private static String getItem(ItemStack stack)
	{
		return (stack.x / TheMazeRunner.Block_SCALE) + separator + (stack.y / TheMazeRunner.Block_SCALE) + separator + stack.getItem().getName() + separator + stack.getAmount();
	}
}
