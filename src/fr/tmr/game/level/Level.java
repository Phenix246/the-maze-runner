package fr.tmr.game.level;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.Display;

import fr.tmr.core.Engine;
import fr.tmr.core.graphics.Camera;
import fr.tmr.core.graphics.Renderer;
import fr.tmr.core.graphics.Texture;
import fr.tmr.core.gui.IScreen;
import fr.tmr.core.utils.GameObject;
import fr.tmr.game.TheMazeRunner;
import fr.tmr.game.block.Block;
import fr.tmr.game.block.BlockList;
import fr.tmr.game.entity.Entity;
import fr.tmr.game.entity.EntityPlayer;
import fr.tmr.game.items.ItemList;
import fr.tmr.game.items.ItemStack;
import fr.tmr.game.pathfinder.Node;

public class Level implements IScreen
{
	private int cycle = 0;

	// Variables du niveau
	public String worldName = "";
	public int worldLimitX = 0;
	public int worldLimitY = 0;
	public boolean isActiveFog = true;
	public boolean isLevelLoaded = false;
	public boolean displayPath = false;

	public Block[][] blockMap;
	// TODO remove
	// private int[] bounds = new int[4];
	// public static float baseX, baseY;

	// Camera
	public Camera camera;

	// Variable aléatoire
	public Random random = new Random();

	// Utils variables
	public int maxTreasure = 0;
	public int maxFog = 0;

	// coordonnées du point d'apparition
	public int xSpawnCoor = 0;
	public int ySpawnCoor = 0;

	// coordonnées de la fin du niveau
	public int xEndCoor = 0;
	public int yEndCoor = 0;

	// Variables pour les entitées
	public ArrayList<Entity> entitiesList;
	public ArrayList<Entity> remove;
	public EntityPlayer player;

	public Level()
	{
		// On initialize les tableaux
		this.entitiesList = new ArrayList<Entity>();
		this.remove = new ArrayList<Entity>();
	}

	// On récupère un bloc en fonction de ses coordonnées
	public Block getBlock(int x, int y)
	{
		if(x >= 0 && x < worldLimitX && y >= 0 && y < worldLimitY)
			if(blockMap[x][y] != null)
				return blockMap[x][y];
		return BlockList.AIR;
	}

	// On place le block demandé aux coordonnées précisées
	public void setBlock(int x, int y, Block block)
	{
		if(x >= 0 && x < worldLimitX && y >= 0 && y < worldLimitY)
			if(block != null)
				blockMap[x][y] = block;
	}

	public Level initWorld(Block[][] map)
	{
		this.worldLimitX = map[0].length;
		this.worldLimitY = map.length;
		this.blockMap = new Block[worldLimitX][worldLimitY];

		// On initialize le joueur
		this.player = new EntityPlayer();
		// On enregistre le joueur
		this.entitiesList.add(player);

		System.out.println("x: " + map[0].length + " y: " + map.length);
		for(int i = 0; i < this.worldLimitX; i++)
			for(int j = 0; j < this.worldLimitY; j++)
			{
				this.setBlock(i, j, map[i][j]);
				if(map[i][j] == BlockList.END)
				{
					this.xEndCoor = i;
					this.yEndCoor = j;
				}
				if(map[i][j] == BlockList.START)
				{
					this.xSpawnCoor = i;
					this.ySpawnCoor = j;
					this.entitiesList.get(0).init(this, xSpawnCoor, ySpawnCoor);
				}
			}

		return this;
	}

	// On créé le niveau
	public Level initWorld(String name)
	{
		System.out.println("name : " + name);
		// on passe la variable isLevelLoaded à fau pour dire que le monde n'est pas prêt
		this.isLevelLoaded = false;

		this.entitiesList.clear();
		this.remove.clear();
		// #FF9900 -> finish
		// #0000FF -> spawn

		// #00FF00 -> diamond
		// #FFFF00 ->
		// #000000 -> Wall
		// #FFFFFF -> Air
		int[] pixels;
		BufferedImage image = null;
		try
		{
			System.out.println(Texture.class.getResource("/assets/levels/" + name + ".png").getPath().replaceAll("%20", " "));
			image = ImageIO.read(Level.class.getResource("/assets/levels/" + name + ".png"));
			this.worldName = name;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		int width = image.getWidth();
		int height = image.getHeight();

		this.worldLimitX = width;
		this.worldLimitY = height;

		this.blockMap = new Block[worldLimitX][worldLimitY];

		// On initialize la caméra
		this.camera = new Camera();

		// On initialize les limites de la vision dans le monde
		this.camera.setBounds(0, 0, ((-this.worldLimitY * TheMazeRunner.Block_SCALE) + Engine.height), ((-this.worldLimitX * TheMazeRunner.Block_SCALE) + Engine.width));
		// bounds[0] = 0;
		// bounds[1] = 0;
		// this.changeBounds();

		pixels = new int[width * height];
		image.getRGB(0, 0, width, height, pixels, 0, width);

		// On initialize le joueur
		this.player = new EntityPlayer();
		// On enregistre le joueur
		this.entitiesList.add(player);

		// On génère le monde
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				// Structure
				// On ajoute les murs
				if(pixels[x + y * width] == 0xFF000000)
				{
					this.setBlock(x, y, BlockList.WALL);
				}
				// On ajoute le sol
				if(pixels[x + y * width] == 0xFFffffff)
				{
					this.setBlock(x, y, BlockList.AIR);
				}

				// Mechanics
				// On ajoute le point d'apparition
				if(pixels[x + y * width] == 0xFF0000ff)
				{
					this.setBlock(x, y, BlockList.START);
					this.xSpawnCoor = x;
					this.ySpawnCoor = y;
					this.entitiesList.get(0).init(this, xSpawnCoor, ySpawnCoor);
				}
				// On ajoute la fin du niveau
				if(pixels[x + y * width] == 0xFFff9900)
				{
					this.setBlock(x, y, BlockList.END);
					this.xEndCoor = x;
					this.yEndCoor = y;
				}

				// Trésors
				// On ajoute les diamants
				if(pixels[x + y * width] == 0xFF00ff00)
				{
					this.setBlock(x, y, BlockList.AIR);
					this.entitiesList.add(new ItemStack(ItemList.DIAMOND, x, y));
					this.maxTreasure++;
				}

				// On vérifie si le brouillard est activé et on l'ajoute
				if(!this.getBlock(x, y).equals(BlockList.WALL) && this.isActiveFog)
				{
					this.entitiesList.add(new ItemStack(ItemList.FOG, x, y));
					this.maxFog++;
				}
			}
		}

		// on passe la variable isLevelLoaded à vrai pour dire que le monde est prêt
		this.isLevelLoaded = true;

		this.player.getPathtoEnd();

		// On retourne le niveau
		return this;
	}

	public static Level loadWorld(String name)
	{
		Level level = MapReader.getMap(name);
		level.isLevelLoaded = true;
		level.entitiesList.get(0).init(level, level.xSpawnCoor, level.ySpawnCoor);

		return level;
	}

	public boolean SaveWorld()
	{
		return MapSaver.saveMap(this);
	}

	// // Get world limits
	// public float getBounds(int i)
	// {
	// if(i >= 0 && i < this.bounds.length)
	// return this.bounds[i];
	// return 0;
	// }

	@Override
	public void render()
	{
		// Test if world is loaded
		if(this.isLevelLoaded)
		{
			// Get new world limits for renderer
			this.camera.setBounds(0, 0, ((-this.worldLimitY * TheMazeRunner.Block_SCALE) + Engine.height - 32), ((-this.worldLimitX * TheMazeRunner.Block_SCALE) + Engine.width));

			// Center the camera on the player
			this.camera.changePosition((-player.x + (Display.getWidth() / 2) - (EntityPlayer.SIZE / 2)), (-player.y + (Display.getHeight() / 2) - (EntityPlayer.SIZE / 2)));

			// Move camera
			this.camera.updatePosition();
			// Render world
			for(int i = 0; i < this.worldLimitX; i++)
			{
				for(int j = 0; j < this.worldLimitY; j++)
				{
					TheMazeRunner.instance.level.getBlock(i, j).render(i, j);
				}
			}

			// Render entities
			for(GameObject go : entitiesList)
			{
				// Render entities
				if(!(go instanceof EntityPlayer) && !(go instanceof ItemStack))
				{
					go.render();
				}

				// Render itemStacks
				if(go instanceof ItemStack)
				{
					ItemStack stack = (ItemStack)go;
					stack.render();
				}
			}

			// render path
			if(displayPath)
			{
				ArrayList<Node> nodes = this.player.pathfinder.getPath();

				if(nodes != null)
					for(Node node : nodes)
					{
						Renderer.renderColor((node.x + 0.1f) * TheMazeRunner.Block_SCALE, (node.y + 0.1f) * TheMazeRunner.Block_SCALE, 0.8F * TheMazeRunner.Block_SCALE, 0.8F * TheMazeRunner.Block_SCALE, new float[] {1, 1, 1});
					}
			}
			// Render player
			player.render();

			this.camera.resetPosition();
		}
	}

	@Override
	public void update(int ticks)
	{
		// Test if world is loaded
		if(this.isLevelLoaded)
		{
			// Get player commands
			this.player.getInput();

			// Get new world limits for renderer
			this.camera.setBounds(0, 0, ((-this.worldLimitY * TheMazeRunner.Block_SCALE) + Engine.height), ((-this.worldLimitX * TheMazeRunner.Block_SCALE) + Engine.width));

			for(Entity go : entitiesList)
			{
				if(!go.isRemove())
					// Update object which aren't remove
					go.update(ticks);
				else
					// Add remove object to a second list to prevent crash
					remove.add(go);
			}

			for(Entity go : remove)
			{
				// Remove object from the first list
				entitiesList.remove(go);
			}

			// Clear remove list
			remove.clear();

			// auto save system
			cycle++;
			if(cycle >= 1000)
			{
				this.SaveWorld();
				cycle = 0;
			}
		}
	}

	// public void changeBounds()
	// {
	// this.bounds[2] = ((-this.worldLimitY * TheMazeRunner.Block_SCALE) + Engine.height);
	// this.bounds[3] = ((-this.worldLimitX * TheMazeRunner.Block_SCALE) + Engine.width);
	// }

}
