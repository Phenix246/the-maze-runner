package fr.tmr.game.level;

import java.util.Random;

import fr.tmr.game.block.Block;
import fr.tmr.game.block.BlockList;
import fr.tmr.game.pathfinder.Node;
import fr.tmr.game.pathfinder.PathfinderManhattan;

public class Generator
{
	private static Block[] blocks = new Block[] {BlockList.AIR, BlockList.WALL, BlockList.END, BlockList.START};

	public static Block[][] gen(int width, int height)
	{
		boolean hasEnd = false, hasStart = false;
		Random rand = new Random();
		Block[][] map = new Block[width][height];
		for(int i = 0; i < width; i++)
			for(int j = 0; j < height; j++)
			{
				int id = rand.nextInt(blocks.length);
				Block b = blocks[id];
				if(b == BlockList.START && !hasStart)
				{
					map[i][j] = b;
					hasStart = true;
				}
				if(b == BlockList.END && !hasEnd)
				{
					map[i][j] = b;
					hasEnd = true;
				}
				if((b == BlockList.END && !hasEnd) || (b == BlockList.START && !hasStart))
					map[i][j] = BlockList.AIR;

				map[i][j] = b;
			}
		return map;
	}

	public static boolean testMap(Level level)
	{
		PathfinderManhattan pathfinder = new PathfinderManhattan(level);
		pathfinder.find(new Node(level.xSpawnCoor, level.ySpawnCoor), new Node(level.xEndCoor, level.yEndCoor));
		return pathfinder.hasPath();
	}
}